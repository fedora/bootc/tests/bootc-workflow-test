#!/bin/bash
set -exuo pipefail

source tools/shared_lib.sh
expand_rootfs
dump_runner
image_inspect

# SSH configurations
SSH_KEY=${TEMPDIR}/id_rsa
ssh-keygen -f "${SSH_KEY}" -N "" -q -t rsa-sha2-256 -b 2048
SSH_KEY_PUB="${SSH_KEY}.pub"

if [[ "$CI" == "RHTAP" ]]; then
  greenprint "Login quay.io"
  sudo podman login -u "${KONFLUX_USERNAME}" -p "${KONFLUX_PASSWORD}" "${TIER1_IMAGE_URL%%@*}"
fi
# konflux for bib image
if [[ ${TIER1_IMAGE_URL} =~ bootc-image-builder ]]; then
  BIB_IMAGE_URL=$TIER1_IMAGE_URL
  if [[ "$REDHAT_ID" == "rhel" ]]; then
    if [[ "$REDHAT_VERSION_ID" == "9.4" ]]; then
      TIER1_IMAGE_URL="registry.stage.redhat.io/rhel${REDHAT_VERSION_ID%%.*}${BETA}/rhel-bootc:rhel-${REDHAT_VERSION_ID%-beta}${BETA}"
    else
      TIER1_IMAGE_URL="registry.stage.redhat.io/rhel${REDHAT_VERSION_ID%%.*}${BETA}/rhel-bootc:${REDHAT_VERSION_ID%-beta}${BETA}"
    fi
  # bib upstream image: quay.io/centos-bootc/bootc-image-builder:latest
  # use centos stream bootc image to test bib quay.io/centos-bootc/centos-bootc:stream9
  else
    TIER1_IMAGE_URL="quay.io/centos-bootc/centos-bootc:stream9"
  fi
# Daily test
else
  if [[ ${REDHAT_VERSION_ID} == *beta ]]; then
    BETA="-beta"
  else
    BETA=""
  fi
  # 9.x bib -> 9.x bootc image; 10-beta bib -> 10-beta bootc image
  if [[ "$REDHAT_ID" == "rhel" ]]; then
    # Set rhel bib image url
    if [[ "$REDHAT_VERSION_ID" == "9.4" ]]; then
      BIB_IMAGE_URL="${BIB_IMAGE_URL:-registry.stage.redhat.io/rhel${REDHAT_VERSION_ID%%.*}${BETA}/bootc-image-builder:rhel-${REDHAT_VERSION_ID%-beta}${BETA}}"
    else
      BIB_IMAGE_URL="${BIB_IMAGE_URL:-registry.stage.redhat.io/rhel${REDHAT_VERSION_ID%%.*}${BETA}/bootc-image-builder:${REDHAT_VERSION_ID%-beta}${BETA}}"
    fi
  # fedora bib -> fedora and centos stream bootc image
  else
    BIB_IMAGE_URL="${BIB_IMAGE_URL:-quay.io/centos-bootc/bootc-image-builder:latest}"
  fi
fi
LAYERED_IMAGE="${LAYERED_IMAGE-cloud-init}"
LAYERED_DIR="examples/$LAYERED_IMAGE"
INSTALL_CONTAINERFILE="$LAYERED_DIR/Containerfile"
UPGRADE_CONTAINERFILE=${TEMPDIR}/Containerfile.upgrade
QUAY_REPO_TAG="${QUAY_REPO_TAG:-$(
  tr -dc a-z0-9 </dev/urandom | head -c 4
  echo ''
)}"
INVENTORY_FILE="${TEMPDIR}/inventory"
# only image type raw, qcow2, anaconda-iso and to-disk use PLATFORM=libvirt in tmt plan
# PLATFORM=libvirt -> use local registry -> no auth required in local registry
# local registry uses libvirt integration network gateway IP
if [[ "$PLATFORM" == "libvirt" ]]; then
  REGISTRY_IP="192.168.100.1"
  REGISTRY_PORT=5000
fi

# Do not enable FIPS except FIPS=enabled
FIPS="${FIPS:-disabled}"
# vhd Azure and gce Google cloud confidential vm test
CONFIDENTIAL_VM="${CONFIDENTIAL_VM:-disabled}"

REPLACE_CLOUD_USER=""
TEST_IMAGE_NAME="bootc-workflow-test"

# Only need quay.io login in non local registry or rhel-bootc-dev image
if [[ "$TIER1_IMAGE_URL" =~ rhel-bootc-dev ]] || [[ "$PLATFORM" != "libvirt" ]]; then
  greenprint "Login quay.io"
  sudo podman login -u "${QUAY_USERNAME}" -p "${QUAY_PASSWORD}" quay.io
fi

case "$REDHAT_ID" in
"rhel")
  # rhel-bootc images are saved in registry.stage.redhat.io
  greenprint "Login registry.stage.redhat.io"
  sudo podman login -u "${REGISTRY_STAGE_USERNAME}" -p "${REGISTRY_STAGE_PASSWORD}" registry.stage.redhat.io
  # work with old TEST_OS variable value rhel-9-x
  TEST_OS=$(echo "${REDHAT_ID}-${REDHAT_VERSION_ID}" | sed 's/\./-/')
  # RHEL image can be saved in private repo only
  TEST_IMAGE_URL="quay.io/redhat_emp1/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  SSH_USER="cloud-user"
  if [[ "$REDHAT_VERSION_ID" == *-beta ]]; then
    STAGE_POSTFIX="-Public-Beta"
  else
    STAGE_POSTFIX=""
  fi
  # use latest compose if specific compose is not accessible
  RC=$(curl -skIw '%{http_code}' -o /dev/null "http://${DOWNLOAD_NODE}/rhel-${REDHAT_VERSION_ID%%.*}/nightly/${BATCH_COMPOSE}RHEL-${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/${CURRENT_COMPOSE_ID}/STATUS")
  if [[ $RC != "200" ]]; then
    CURRENT_COMPOSE_ID=latest-RHEL-${REDHAT_VERSION_ID%%-beta}
  fi
  sed "s/REPLACE_ME/${DOWNLOAD_NODE}/; s/REPLACE_X/${REDHAT_VERSION_ID%%.*}/; s|REPLACE_BATCH_COMPOSE|${BATCH_COMPOSE}|; s/REPLACE_Y/${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/; s/REPLACE_COMPOSE_ID/${CURRENT_COMPOSE_ID}/" files/rhel.template | tee "${LAYERED_DIR}"/rhel.repo >/dev/null
  ADD_REPO="COPY rhel.repo /etc/yum.repos.d/rhel.repo"
  ADD_RHC="RUN dnf install -y rhc"
  if [[ "$PLATFORM" == "aws" ]]; then
    SSH_USER="ec2-user"
    REPLACE_CLOUD_USER='RUN sed -i "s/name: cloud-user/name: ec2-user/g" /etc/cloud/cloud.cfg'
  fi
  BOOT_ARGS="uefi"
  ;;
"centos")
  # work with old TEST_OS variable value centos-stream-9
  TEST_OS=$(echo "${REDHAT_ID}-${REDHAT_VERSION_ID}" | sed 's/-/-stream-/')
  TEST_IMAGE_URL="quay.io/bootc-test/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  SSH_USER="cloud-user"
  ADD_REPO=""
  ADD_RHC=""
  if [[ "$PLATFORM" == "aws" ]]; then
    SSH_USER="ec2-user"
    REPLACE_CLOUD_USER='RUN sed -i "s/name: cloud-user/name: ec2-user/g" /etc/cloud/cloud.cfg'
  fi
  BOOT_ARGS="uefi,firmware.feature0.name=secure-boot,firmware.feature0.enabled=no"
  ;;
"fedora")
  TEST_OS="${REDHAT_ID}-${REDHAT_VERSION_ID}"
  TEST_IMAGE_URL="quay.io/bootc-test/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
  SSH_USER="fedora"
  ADD_REPO=""
  ADD_RHC=""
  BOOT_ARGS="uefi"
  ;;
*)
  redprint "Variable TIER1_IMAGE_URL is not supported"
  exit 1
  ;;
esac

greenprint "Configure container build arch"
if [[ "$CROSS_ARCH" == "True" ]]; then
  if [[ "$ARCH" == "x86_64" ]]; then
    ARCH="aarch64"
  else
    ARCH="x86_64"
  fi
fi

case "$ARCH" in
"x86_64")
  BUILD_PLATFORM="linux/amd64"
  ;;
"aarch64")
  BUILD_PLATFORM="linux/arm64"
  ;;
"s390x")
  BUILD_PLATFORM="linux/s390x"
  ;;
"ppc64le")
  BUILD_PLATFORM="linux/ppc64le"
  ;;
esac

greenprint "Create ${TEST_OS} installation Containerfile"
sed -i "s|^FROM.*|FROM $TIER1_IMAGE_URL\n$ADD_REPO\n$ADD_RHC|" "$INSTALL_CONTAINERFILE"

if [[ "$LAYERED_IMAGE" == "useradd-ssh" ]]; then
  sed -i "s|exampleuser|$SSH_USER|g" "$INSTALL_CONTAINERFILE"
fi

# to-disk test for fips enable or not will use root user
if [[ "$IMAGE_TYPE" == "to-disk" ]]; then
  SSH_USER="root"
fi

# for qcow2 image and fips enabled, add cloud-init
if [[ "$LAYERED_IMAGE" == "fips" ]] && [[ "$IMAGE_TYPE" == "qcow2" ]]; then
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN dnf -y install cloud-init && \
    ln -s ../cloud-init.target /usr/lib/systemd/system/default.target.wants && \
    mkdir -p /usr/lib/bootc/kargs.d && \
    echo 'kargs = ["console=tty0", "console=ttyS0,115200n8"]' > /usr/lib/bootc/kargs.d/05-cloud.toml
EOF
fi

# add crashkernel for qcow2 image
if [[ "$LAYERED_IMAGE" != "fips" ]] && [[ "$IMAGE_TYPE" == "qcow2" ]] && [[ "$REDHAT_ID" != "fedora" ]]; then
  CRASHKERNEL=$(sudo podman run --rm --tls-verify=false "$TIER1_IMAGE_URL" kdumpctl get-default-crashkernel)
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN mkdir -p /usr/lib/bootc/kargs.d && \
    echo 'kargs = ["crashkernel=$CRASHKERNEL"]' > /usr/lib/bootc/kargs.d/05-cloud.toml
EOF
fi

# only image type raw, qcow2, anaconda-iso and to-disk use PLATFORM=libvirt in tmt plan
# PLATFORM=libvirt -> use local registry -> no auth required in local registry
if [[ "$PLATFORM" == "libvirt" ]]; then
  # Init libvirt network
  greenprint "Deploy libvirt network"
  deploy_libvirt_network

  greenprint "Generate certificate"
  openssl req \
    -newkey rsa:4096 \
    -nodes \
    -sha256 \
    -keyout "${TEMPDIR}/domain.key" \
    -addext "subjectAltName = IP:${REGISTRY_IP}" \
    -x509 \
    -days 365 \
    -out "${TEMPDIR}/domain.crt" \
    -subj "/C=US/ST=Denial/L=Stockholm/O=bootc/OU=bootc-test/CN=bootc-test/emailAddress=bootc-test@bootc-test.org"

  greenprint "Update CA Trust"
  sudo cp "${TEMPDIR}/domain.crt" "/etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt"
  sudo update-ca-trust

  greenprint "Deploy registry"
  podman run \
    -d \
    --name registry \
    --replace \
    --network host \
    -v "${TEMPDIR}":/certs:z \
    -e REGISTRY_HTTP_ADDR="${REGISTRY_IP}:${REGISTRY_PORT}" \
    -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
    -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
    quay.io/bootc-test/registry:2.8.3
  podman ps -a

  cp "${TEMPDIR}/domain.crt" "$LAYERED_DIR"
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
COPY domain.crt /etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt
RUN update-ca-trust && \
    dnf -y clean all
EOF

  TEST_IMAGE_URL="${REGISTRY_IP}:${REGISTRY_PORT}/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"
else
  tee -a "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
RUN dnf -y clean all
$REPLACE_CLOUD_USER
EOF
fi

if [[ "$IMAGE_TYPE" == "vmdk" ]]; then
  greenprint "Install cloud-init for vmdk image"
  sed -i "s/open-vm-tools/cloud-init open-vm-tools/" "$INSTALL_CONTAINERFILE"
fi

# No google-compute-engine for RHEL 10 and CS10
if [[ "$IMAGE_TYPE" == "gce" ]] && [[ "$REDHAT_VERSION_ID" =~ "10" ]]; then
  greenprint "Use cloud-init for RHEL 10 and CS10 instand of google compute engine"
  sed -i "s/google-compute-engine/cloud-init/" "$INSTALL_CONTAINERFILE"
fi

greenprint "Check $TEST_OS installation Containerfile"
cat "$INSTALL_CONTAINERFILE"

greenprint "Build $TEST_OS installation container image"
if [[ "$LAYERED_IMAGE" == "useradd-ssh" ]]; then
  sudo podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" --build-arg "sshpubkey=$(cat "${SSH_KEY_PUB}")" -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$LAYERED_DIR"
else
  sudo podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$LAYERED_DIR"
fi

# only image type raw, qcow2, anaconda-iso and to-disk use PLATFORM=libvirt in tmt plan
# PLATFORM=libvirt -> use local registry -> no auth required in local registry
if [[ "$PLATFORM" != "libvirt" ]]; then
  [[ $- =~ x ]] && debug=1 && set +x
  sed "s/REPLACE_ME/${QUAY_SECRET}/g" files/auth.template | tee auth.json >/dev/null
  [[ $debug == 1 ]] && set -x
  sudo podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" --from "localhost/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" --secret id=creds,src=./auth.json -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" examples/container-auth
fi

greenprint "Push $TEST_OS installation container image"
sudo podman push --tls-verify=false --quiet "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$TEST_IMAGE_URL"

greenprint "Prepare inventory file"
tee -a "$INVENTORY_FILE" >/dev/null <<EOF
[cloud]
localhost

[guest]

[cloud:vars]
ansible_connection=local

[guest:vars]
ansible_user="$SSH_USER"
ansible_private_key_file="$SSH_KEY"
ansible_ssh_common_args="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

[all:vars]
ansible_python_interpreter=/usr/bin/python3
EOF

greenprint "Prepare ansible.cfg"
export ANSIBLE_CONFIG="${PWD}/playbooks/ansible.cfg"

greenprint "Configure rootfs randomly"
if [[ "$REDHAT_ID" == fedora ]]; then
  ROOTFS_LIST=(
    "ext4"
    "xfs"
    "btrfs"
  )
  RND_LINE=$((RANDOM % 3))
else
  ROOTFS_LIST=(
    "ext4"
    "xfs"
  )
  RND_LINE=$((RANDOM % 2))
fi
ROOTFS="${ROOTFS_LIST[$RND_LINE]}"

case "$IMAGE_TYPE" in
"ami")
  greenprint "Build $TEST_OS $IMAGE_TYPE image"
  AMI_NAME="bootc-bib-${TEST_OS}-${ARCH}-${QUAY_REPO_TAG}"
  AWS_BUCKET_NAME="bootc-bib-ami-test"
  sudo podman pull --tls-verify=false "$TEST_IMAGE_URL"
  sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    --env AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" \
    --env AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" \
    "$BIB_IMAGE_URL" \
    --type ami \
    --target-arch "$ARCH" \
    --aws-ami-name "$AMI_NAME" \
    --aws-bucket "$AWS_BUCKET_NAME" \
    --aws-region "$AWS_REGION" \
    --rootfs "$ROOTFS" \
    --use-librepo \
    --verbose \
    "$TEST_IMAGE_URL"

  greenprint "Get uploaded AMI ID and snapshot ID"
  AMI_ID=$(
    aws ec2 describe-images \
      --filters "Name=name,Values=${AMI_NAME}" \
      --query 'Images[*].ImageId' \
      --output text
  )

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e inventory_file="$INVENTORY_FILE" \
    -e ami_id="$AMI_ID" \
    "playbooks/deploy-aws.yaml"
  ;;
"qcow2" | "raw")
  greenprint "Build $TEST_OS $IMAGE_TYPE image"
  mkdir -p output
  sudo podman pull --tls-verify=false "$TEST_IMAGE_URL"
  # qcow2 and raw images use local registry and no auth required
  sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "$(pwd)/output":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type "$IMAGE_TYPE" \
    --target-arch "$ARCH" \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --use-librepo \
    --verbose \
    "$TEST_IMAGE_URL"

  if [[ "$IMAGE_TYPE" == "raw" ]]; then
    qemu-img convert -f raw output/image/disk.raw -O qcow2 output/image/disk.qcow2
    sudo mv output/image/disk.qcow2 /var/lib/libvirt/images && sudo rm -rf output
    # raw image will run test on bios vm for x86_64
    if [[ "$ARCH" == "x86_64" ]]; then
      BIB_FIRMWARE=bios
    elif [[ "$ARCH" == "s390x" ]]; then
      BIB_FIRMWARE=""
    else
      BIB_FIRMWARE=uefi
    fi
  else
    sudo mv output/qcow2/disk.qcow2 /var/lib/libvirt/images && sudo rm -rf output
    # qcow2 image will run test on uefi vm
    if [[ "$ARCH" == "s390x" ]]; then
      BIB_FIRMWARE=""
    else
      BIB_FIRMWARE=uefi
    fi
  fi

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e ssh_user="$SSH_USER" \
    -e inventory_file="$INVENTORY_FILE" \
    -e bib="true" \
    -e boot_args="$BOOT_ARGS" \
    -e bib_firmware="$BIB_FIRMWARE" \
    "playbooks/deploy-libvirt.yaml"
  ;;
"vmdk")
  mkdir -p output

  greenprint "Build $TEST_OS $IMAGE_TYPE image"
  sudo podman pull --tls-verify=false "$TEST_IMAGE_URL"
  sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "$(pwd)/output":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type vmdk \
    --target-arch "$ARCH" \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --use-librepo \
    --verbose \
    "$TEST_IMAGE_URL"

  greenprint "📋 Rename vmdk file name"
  mv output/vmdk/disk.vmdk "${TEMPDIR}/disk.vmdk"
  sudo rm -rf output

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e redhat_id="$REDHAT_ID" \
    -e ssh_user="$SSH_USER" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e inventory_file="$INVENTORY_FILE" \
    -e bib="true" \
    "playbooks/deploy-vmware.yaml"
  ;;
"anaconda-iso")
  greenprint "Build $TEST_OS $IMAGE_TYPE image"
  mkdir -p output
  sudo podman pull --tls-verify=false "$TEST_IMAGE_URL"
  sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "$(pwd)/output":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type "$IMAGE_TYPE" \
    --target-arch "$ARCH" \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --use-librepo \
    --verbose \
    "$TEST_IMAGE_URL"

  ISOMOUNT=$(mktemp -d)

  greenprint "Mounting install.iso -> $ISOMOUNT"
  sudo mount -v -o ro "output/bootiso/install.iso" "$ISOMOUNT"

  NEW_KS_FILE="${TEMPDIR}/bib.ks"

  greenprint "Default osbuild-base.ks"
  cat "${ISOMOUNT}/osbuild-base.ks"
  greenprint "Default osbuild.ks"
  cat "${ISOMOUNT}/osbuild.ks"

  greenprint "Preparing modified kickstart file"
  cat >"$NEW_KS_FILE" <<EOFKS
text
$(cat "${ISOMOUNT}/osbuild-base.ks")
$(cat "${ISOMOUNT}/osbuild.ks")
EOFKS
  sed -i '/%include/d' "$NEW_KS_FILE"

  greenprint "Writing new ISO"
  sudo mkksiso -c "inst.sshd console=ttyS0,115200" --ks "$NEW_KS_FILE" "output/bootiso/install.iso" "/var/lib/libvirt/images/install.iso"

  greenprint "==== NEW KICKSTART FILE ===="
  cat "$NEW_KS_FILE"
  greenprint "============================"

  greenprint "Clean up ISO building environment"
  sudo umount -v "$ISOMOUNT"
  rm -rf "$ISOMOUNT"
  sudo rm -rf output

  greenprint "💾 Create vm qcow2 files for ISO installation"
  sudo qemu-img create -f qcow2 "/var/lib/libvirt/images/disk.qcow2" 10G

  if [[ "$ARCH" == "x86_64" ]]; then
    BIB_FIRMWARE_LIST=(
      "bios"
      "uefi"
    )
    RND_LINE=$((RANDOM % 2))
    BIB_FIRMWARE="${BIB_FIRMWARE_LIST[$RND_LINE]}"
  elif [[ "$ARCH" == "s390x" ]]; then
    BIB_FIRMWARE=""
  else
    BIB_FIRMWARE="uefi"
  fi

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e ssh_user="$SSH_USER" \
    -e inventory_file="$INVENTORY_FILE" \
    -e bib="true" \
    -e bib_firmware="$BIB_FIRMWARE" \
    -e boot_args="$BOOT_ARGS" \
    -e image_type="$IMAGE_TYPE" \
    "playbooks/deploy-libvirt.yaml"
  ;;
"to-disk")
  greenprint "💾 Create disk.raw"
  sudo truncate -s 10G disk.raw

  # Reuse /output folder for id_rsa.pub
  cp "$SSH_KEY_PUB" .

  greenprint "bootc install to disk.raw"
  sudo podman run \
    --rm \
    --privileged \
    --tls-verify=false \
    --pid=host \
    --security-opt label=type:unconfined_t \
    -v /var/lib/containers:/var/lib/containers \
    -v /dev:/dev \
    -v .:/output \
    -e BOOTC_SETENFORCE0_FALLBACK=1 \
    "$TEST_IMAGE_URL" \
    bootc install to-disk \
    --filesystem "$ROOTFS" \
    --root-ssh-authorized-keys "/output/id_rsa.pub" \
    --generic-image \
    --via-loopback \
    /output/disk.raw

  sudo qemu-img convert -f raw ./disk.raw -O qcow2 "/var/lib/libvirt/images/disk.qcow2"
  rm -f disk.raw id_rsa.pub

  if [[ "$ARCH" == "x86_64" ]]; then
    BIB_FIRMWARE_LIST=(
      "bios"
      "uefi"
    )
    RND_LINE=$((RANDOM % 2))
    BIB_FIRMWARE="${BIB_FIRMWARE_LIST[$RND_LINE]}"
  elif [[ "$ARCH" == "s390x" ]]; then
    BIB_FIRMWARE=""
  else
    BIB_FIRMWARE="uefi"
  fi

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e ssh_user="$SSH_USER" \
    -e inventory_file="$INVENTORY_FILE" \
    -e bib="true" \
    -e bib_firmware="$BIB_FIRMWARE" \
    -e boot_args="$BOOT_ARGS" \
    -e image_type="$IMAGE_TYPE" \
    "playbooks/deploy-libvirt.yaml"
  ;;
"vhd")
  # vhd is not available in rhel-9.4 bib image, use upstream bib
  if [[ "$REDHAT_VERSION_ID" == "9.4" ]]; then
    BIB_IMAGE_URL="quay.io/centos-bootc/bootc-image-builder:latest"
  fi
  mkdir -p output
  greenprint "Build $TEST_OS $IMAGE_TYPE image"
  sudo podman pull --tls-verify=false "$TEST_IMAGE_URL"
  sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "$(pwd)/output":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type vhd \
    --target-arch "$ARCH" \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --use-librepo \
    --verbose \
    "$TEST_IMAGE_URL"

  greenprint "Upload vhd image to Azure"

  AZURE_RG="rg_bib_${QUAY_REPO_TAG}"
  AZURE_LOCATION=eastus
  AZURE_SA="bibstorage${QUAY_REPO_TAG}"
  AZURE_SC="bib${QUAY_REPO_TAG}"
  AZURE_GALLERY_NAME="bib_vhd_${QUAY_REPO_TAG}"
  AZURE_GALLERY_IMAGE_DEFINITION="bib_image_${QUAY_REPO_TAG}"
  AZURE_PUBLISHER="bootc_qe"
  AZURE_OFFER="bib_vhd_test"
  AZURE_SKU="bootc_qe"
  VHD_FILE="disk.vhd"

  az login \
    --service-principal \
    -u "$AZURE_CLIENT_ID" \
    -p "$AZURE_SECRET" \
    --tenant "$AZURE_TENANT"

  az group create \
    --name "$AZURE_RG" \
    --location "$AZURE_LOCATION" \
    --tags "project=bib"

  az group wait \
    --created \
    --resource-group "$AZURE_RG" \
    --name "$AZURE_RG"

  az storage account create \
    --resource-group "$AZURE_RG" \
    --name "$AZURE_SA" \
    --location "$AZURE_LOCATION" \
    --access-tier Hot \
    --sku Standard_LRS

  # Wait for storage account created
  for _ in $(seq 0 180); do
    SC_NAME=$(
      az storage account list \
        -g "$AZURE_RG" \
        --output json | jq -r '.[0].name'
    )

    # Has the storage account created?
    if [[ "$SC_NAME" == "$AZURE_SA" ]]; then
      break
    fi

    # Wait 10 seconds and try again.
    sleep 10
  done

  az storage container create \
    --account-name "$AZURE_SA" \
    --name "$AZURE_SC"

  # Calculate the disk.vhd md5 and used by image upload validate
  VHD_MD5=$(md5sum "$(pwd)/output/vpc/disk.vhd" | awk '{print $1}')

  az storage blob upload \
    --account-name "$AZURE_SA" \
    --container-name "$AZURE_SC" \
    --file "$(pwd)/output/vpc/disk.vhd" \
    --name "$VHD_FILE" \
    --type page \
    --content-md5 "$VHD_MD5" \
    --validate-content \
    --no-progress

  rm -rf output

  az sig create \
    --location "$AZURE_LOCATION" \
    --resource-group "$AZURE_RG" \
    --gallery-name "$AZURE_GALLERY_NAME"

  if [[ "$ARCH" == x86_64 ]]; then
    VHD_ARCH="x64"
  else
    VHD_ARCH="Arm64"
  fi

  az sig image-definition create \
    --location "$AZURE_LOCATION" \
    --resource-group "$AZURE_RG" \
    --gallery-name "$AZURE_GALLERY_NAME" \
    --gallery-image-definition "$AZURE_GALLERY_IMAGE_DEFINITION" \
    --publisher "$AZURE_PUBLISHER" \
    --offer "$AZURE_OFFER" \
    --sku "$AZURE_SKU" \
    --os-type Linux \
    --os-state Generalized \
    --architecture "$VHD_ARCH" \
    --hyper-v-generation V2 \
    --features IsAcceleratedNetworkSupported=true \
    --features SecurityType=ConfidentialVmSupported

  az sig image-version create \
    --location "$AZURE_LOCATION" \
    --resource-group "$AZURE_RG" \
    --gallery-name "$AZURE_GALLERY_NAME" \
    --gallery-image-definition "$AZURE_GALLERY_IMAGE_DEFINITION" \
    --gallery-image-version 1.0.0 \
    --os-vhd-storage-account "$AZURE_SA" \
    --os-vhd-uri "https://${AZURE_SA}.blob.core.windows.net/${AZURE_SC}/${VHD_FILE}"

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e redhat_id="$REDHAT_ID" \
    -e ssh_user="$SSH_USER" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e inventory_file="$INVENTORY_FILE" \
    -e bib="true" \
    -e rg_image="$AZURE_RG" \
    -e gallery_name="$AZURE_GALLERY_NAME" \
    -e image_definition="$AZURE_GALLERY_IMAGE_DEFINITION" \
    -e confidential_vm="$CONFIDENTIAL_VM" \
    "playbooks/deploy-azure.yaml"
  ;;
"gce")
  # gce is not available in rhel-9.4 bib image, use upstream bib
  if [[ "$REDHAT_VERSION_ID" == "9.4" ]]; then
    BIB_IMAGE_URL="quay.io/centos-bootc/bootc-image-builder:latest"
  fi
  # install python 3.11.9 required by gsutil
  # gsutil versions 5.0 and up require Python 3.5-3.11
  INSTALLED_PYTHON=$(rpm -q --qf "%{version}" "python3")
  sudo dnf install -y rpmdevtools
  set +e
  rpmdev-vercmp "${INSTALLED_PYTHON}" "3.12.0" >/dev/null 2>&1
  # 0 - python version == 3.12.0
  # 11 - python version > 3.12.0
  # 12 - python version < 3.12.0
  if [[ "$?" != "12" ]]; then
    dnf install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel libpcap-devel xz-devel expat-devel curl
    curl https://pyenv.run | bash
    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    pyenv install 3.11.9
    pyenv global 3.11.9
  fi
  set -e
  mkdir -p output
  greenprint "Build $TEST_OS $IMAGE_TYPE image"
  sudo podman pull --tls-verify=false "$TEST_IMAGE_URL"
  sudo podman run \
    --rm \
    -it \
    --privileged \
    --pull=newer \
    --tls-verify=false \
    --security-opt label=type:unconfined_t \
    -v "$(pwd)/output":/output \
    -v /var/lib/containers/storage:/var/lib/containers/storage \
    "$BIB_IMAGE_URL" \
    --type gce \
    --target-arch "$ARCH" \
    --chown "$(id -u "$(whoami)"):$(id -g "$(whoami)")" \
    --rootfs "$ROOTFS" \
    --use-librepo \
    --verbose \
    "$TEST_IMAGE_URL"

  greenprint "Upload vhd image to Azure"

  GCP_BUCKET_NAME="bootc_bucket_${QUAY_REPO_TAG}"
  GCE_FILE="image.tar.gz"
  if [[ "$ARCH" == "aarch64" ]]; then
    GCE_ARCH="ARM64"
    GUEST_OS_FEATURES="UEFI_COMPATIBLE,GVNIC,VIRTIO_SCSI_MULTIQUEUE"
    ARCH_FOR_IMAGE_NAME=aarch64
  elif [[ "$ARCH" == x86_64 ]]; then
    GCE_ARCH="X86_64"
    GUEST_OS_FEATURES="UEFI_COMPATIBLE,GVNIC,SEV_CAPABLE,SEV_SNP_CAPABLE,VIRTIO_SCSI_MULTIQUEUE"
    ARCH_FOR_IMAGE_NAME=x86
  fi
  GCP_IMAGE_NAME="bootc-${TEST_OS}-${ARCH_FOR_IMAGE_NAME}-${QUAY_REPO_TAG}"

  gcloud auth \
    activate-service-account \
    --key-file="$GCP_SERVICE_ACCOUNT_FILE"

  # Create a new cloud storage bucket
  gcloud storage buckets \
    create gs://"${GCP_BUCKET_NAME}" \
    --project "$GCP_PROJECT"

  # Calculate the image.tgz md5 and used by image upload validate
  GCE_MD5=$(gsutil -q hash -m "$(pwd)/output/gce/${GCE_FILE}" | tr -d '\n\t' | rev | cut -d':' -f 1 | rev)

  # Upload image.tgz file to the new bucket
  gsutil -h Content-MD5:"$GCE_MD5" \
    cp "$(pwd)/output/gce/${GCE_FILE}" gs://"${GCP_BUCKET_NAME}"

  # Import the image file as a new custom image
  gcloud compute images \
    create "$GCP_IMAGE_NAME" \
    --source-uri "gs://${GCP_BUCKET_NAME}/${GCE_FILE}" \
    --architecture "$GCE_ARCH" \
    --project "$GCP_PROJECT" \
    --guest-os-features="$GUEST_OS_FEATURES" \
    --labels "project=bootc,test_os=$TEST_OS,arch=$ARCH"

  # Delete objects and bucket
  gcloud storage \
    rm \
    --recursive \
    gs://"$GCP_BUCKET_NAME" \
    --project "$GCP_PROJECT"

  greenprint "Deploy $IMAGE_TYPE instance"
  ansible-playbook -v \
    -i "$INVENTORY_FILE" \
    -e test_os="$TEST_OS" \
    -e redhat_id="$REDHAT_ID" \
    -e ssh_user="$SSH_USER" \
    -e ssh_key_pub="$SSH_KEY_PUB" \
    -e inventory_file="$INVENTORY_FILE" \
    -e bib="true" \
    -e image_name="$GCP_IMAGE_NAME" \
    -e confidential_vm="$CONFIDENTIAL_VM" \
    "playbooks/deploy-gcp.yaml"
  ;;
*)
  redprint "Variable IMAGE_TYPE has to be defined"
  exit 1
  ;;
esac

greenprint "Run ostree checking test on $PLATFORM instance"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e bootc_image="$TEST_IMAGE_URL" \
  -e layered_image="$LAYERED_IMAGE" \
  -e image_label_version_id="${REDHAT_VERSION_ID%-beta}" \
  -e fips="$FIPS" \
  playbooks/check-system.yaml

greenprint "Create upgrade Containerfile"
tee "$UPGRADE_CONTAINERFILE" >/dev/null <<EOF
FROM "$TEST_IMAGE_URL"
RUN dnf -y install wget && \
    dnf -y clean all
EOF

greenprint "Build $TEST_OS upgrade container image"
sudo podman build --platform "$BUILD_PLATFORM" --tls-verify=false --retry=5 --retry-delay=10s --label="quay.expires-after=8h" -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" -f "$UPGRADE_CONTAINERFILE" .
greenprint "Push $TEST_OS upgrade container image"
sudo podman push --tls-verify=false --quiet "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$TEST_IMAGE_URL"

greenprint "Upgrade $TEST_OS system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e bootc_image="$TEST_IMAGE_URL" \
  playbooks/upgrade.yaml

greenprint "Run ostree checking test after upgrade on $PLATFORM instance"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e bootc_image="$TEST_IMAGE_URL" \
  -e image_label_version_id="${REDHAT_VERSION_ID%-beta}" \
  -e fips="$FIPS" \
  -e upgrade="true" \
  playbooks/check-system.yaml

greenprint "Rollback $TEST_OS system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  playbooks/rollback.yaml

greenprint "Clean up"
rm -rf auth.json "${LAYERED_DIR}/rhel.repo"
unset ANSIBLE_CONFIG
podman rm -af

greenprint "🎉 All tests passed."
exit 0
