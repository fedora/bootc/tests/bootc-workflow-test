#!/bin/bash
# DOWNLOAD_NODE=foobar AWS_ACCESS_KEY_ID=foobar AWS_SECRET_ACCESS_KEY=foobar AWS_REGION=us-east-1 ./publish_ami.sh

set -euox pipefail

ARCH=$(uname -m)

# Get OS data.
source /etc/os-release

TEMPDIR=$(mktemp -d)
BLUEPRINT_FILE=${TEMPDIR}/blueprint.toml
COMPOSE_START=${TEMPDIR}/compose-start.json
COMPOSE_INFO=${TEMPDIR}/compose-info.json

sudo dnf install -y --nogpgcheck osbuild osbuild-composer composer-cli curl jq podman

# Customize repository
sudo mkdir -p /etc/osbuild-composer/repositories

case "${ID}-${VERSION_ID}" in
    "rhel-9.6")
        TEST_OS=rhel-9-6
        sed "s/REPLACE_ME_HERE/${DOWNLOAD_NODE}/g" rhel-9-6-0.json | sudo tee /etc/osbuild-composer/repositories/rhel-9.6.json > /dev/null;;
    "rhel-10.0")
        TEST_OS=rhel-10-0
        sed "s/REPLACE_ME_HERE/${DOWNLOAD_NODE}/g" rhel-10-0.json | sudo tee /etc/osbuild-composer/repositories/rhel-10.0.json > /dev/null;;
esac

# Start osbuild-composer.socket
sudo systemctl enable --now osbuild-composer.socket

# Source checking
sudo composer-cli sources list
for SOURCE in $(sudo composer-cli sources list); do
    sudo composer-cli sources info "$SOURCE"
done

# Write a blueprint for ostree image.
tee "$BLUEPRINT_FILE" > /dev/null << EOF
name = "ami"
description = "AWS EC2 image"
version = "0.0.1"
modules = []
groups = []

[[packages]]
name = "podman"
version = "*"
EOF

# Prepare the blueprint for the compose.
sudo composer-cli blueprints push "$BLUEPRINT_FILE"
sudo composer-cli blueprints depsolve ami

# Get worker unit file so we can watch the journal.
WORKER_UNIT=$(sudo systemctl list-units | grep -o -E "osbuild.*worker.*\.service")
sudo journalctl -af -n 1 -u "${WORKER_UNIT}" &

sudo composer-cli --json compose start ami ami | tee "$COMPOSE_START"
COMPOSE_ID=$(jq -r ".[0].body.build_id" "$COMPOSE_START")

# Wait for the compose to finish.
while true; do
    sudo composer-cli --json compose info "${COMPOSE_ID}" | tee "$COMPOSE_INFO" > /dev/null
    COMPOSE_STATUS=$(jq -r ".[0].body.queue_status" "$COMPOSE_INFO")

    # Is the compose finished?
    if [[ $COMPOSE_STATUS != RUNNING ]] && [[ $COMPOSE_STATUS != WAITING ]]; then
        break
    fi

    # Wait 30 seconds and try again.
    sleep 30
done

sudo composer-cli compose image "$COMPOSE_ID"

# Grant all read permission
sudo chmod a+r "${COMPOSE_ID}-image.raw"

# Remove blueprints and compose
sudo composer-cli compose delete "$COMPOSE_ID"
sudo composer-cli compose status
sudo composer-cli blueprints delete ami
sudo composer-cli blueprints list

# Install aws-cli
# curl "https://awscli.amazonaws.com/awscli-exe-linux-$(uname -m).zip" -o "awscliv2.zip" && unzip awscliv2.zip && sudo ./aws/install

UNIQUE_STRING=$(tr -dc a-z0-9 < /dev/urandom | head -c 4 ; echo '')
BUCKET_NAME="bootc-${TEST_OS}-${UNIQUE_STRING}"
BUCKET_URL="s3://${BUCKET_NAME}"
IMAGE_FILENAME="${COMPOSE_ID}-image.raw"

# Create Bucket
aws s3 mb "$BUCKET_URL"

# Upload AMI image to bucket
aws s3 cp \
    --quiet \
    "$IMAGE_FILENAME" \
    "${BUCKET_URL}/" \
    --acl private

# Create container simple file
CONTAINERS_FILE="${TEMPDIR}/containers.json"

tee "$CONTAINERS_FILE" > /dev/null << EOF
{
  "Description": "$IMAGE_FILENAME",
  "Format": "raw",
  "Url": "${BUCKET_URL}/${IMAGE_FILENAME}"
}
EOF

# Import the image as an EBS snapshot into EC2
IMPORT_TASK_ID=$(
    aws ec2 import-snapshot \
        --output json \
        --description "bootc $TEST_OS ami snapshot" \
        --disk-container file://"${CONTAINERS_FILE}" | \
        jq -r '.ImportTaskId'
)
rm -f "$CONTAINERS_FILE"

# Wait for snapshot import complete
for _ in $(seq 0 180); do
    IMPORT_STATUS=$(
        aws ec2 describe-import-snapshot-tasks \
            --output json \
            --import-task-ids "$IMPORT_TASK_ID" | \
            jq -r '.ImportSnapshotTasks[].SnapshotTaskDetail.Status'
    )

    # Has the snapshot finished?
    if [[ $IMPORT_STATUS != active ]]; then
        break
    fi

    # Wait 10 seconds and try again.
    sleep 10
done

if [[ $IMPORT_STATUS != completed ]]; then
    echo "Something went wrong with the snapshot. 😢"
    exit 1
else
    echo "Snapshot imported successfully."
fi

SNAPSHOT_ID=$(
    aws ec2 describe-import-snapshot-tasks \
        --output json \
        --import-task-ids "$IMPORT_TASK_ID" | \
        jq -r '.ImportSnapshotTasks[].SnapshotTaskDetail.SnapshotId'
)

aws ec2 create-tags \
    --resources "$SNAPSHOT_ID" \
    --tags Key=Name,Value="bootc-${TEST_OS}-${ARCH}" Key=ImageName,Value="$IMAGE_FILENAME"

REGISTERED_AMI_NAME="bootc-${TEST_OS}-${ARCH}-$(date +'%y%m%d')"

if [[ "$ARCH" == x86_64 ]]; then
    IMG_ARCH="$ARCH"
elif [[ "$ARCH" == aarch64 ]]; then
    IMG_ARCH=arm64
fi

AMI_ID=$(
    aws ec2 register-image \
        --name "$REGISTERED_AMI_NAME" \
        --root-device-name /dev/xvda \
        --architecture "$IMG_ARCH" \
        --ena-support \
        --sriov-net-support simple \
        --virtualization-type hvm \
        --block-device-mappings DeviceName=/dev/xvda,Ebs=\{SnapshotId="${SNAPSHOT_ID}"\} DeviceName=/dev/xvdf,Ebs=\{VolumeSize=10\} \
        --boot-mode "uefi-preferred" \
        --output json | \
        jq -r '.ImageId'
)

aws ec2 wait image-available \
    --image-ids "$AMI_ID"
aws ec2 create-tags \
    --resources "$AMI_ID" \
    --tags Key=Name,Value="bootc-${TEST_OS}-${ARCH}" Key=ImageName,Value="$IMAGE_FILENAME"

# Remove bucket content and bucket itself quietly
aws s3 rb "$BUCKET_URL" --force > /dev/null

# Save AMI ID to ssm parameter
aws ssm put-parameter \
    --name "bootc-${TEST_OS}-${ARCH}" \
    --type "String" \
    --data-type "aws:ec2:image" \
    --value "$AMI_ID" \
    --overwrite
