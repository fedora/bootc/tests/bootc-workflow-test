#!/bin/bash

# Dumps details about the instance running the CI job.
function dump_runner {
  RUNNER_CPUS=$(nproc)
  RUNNER_MEM=$(free -m | grep -oP '\d+' | head -n 1)
  RUNNER_DISK=$(df --output=size -h / | sed '1d;s/[^0-9]//g')
  RUNNER_HOSTNAME=$(uname -n)
  RUNNER_USER=$(whoami)
  RUNNER_ARCH=$(uname -m)
  RUNNER_KERNEL=$(uname -r)

  echo -e "\033[0;36m"
  cat <<EOF
------------------------------------------------------------------------------
CI MACHINE SPECS
------------------------------------------------------------------------------
    Hostname: ${RUNNER_HOSTNAME}
        User: ${RUNNER_USER}
        CPUs: ${RUNNER_CPUS}
         RAM: ${RUNNER_MEM} MB
        DISK: ${RUNNER_DISK} GB
        ARCH: ${RUNNER_ARCH}
      KERNEL: ${RUNNER_KERNEL}
------------------------------------------------------------------------------
EOF
}

# Colorful timestamped output.
function greenprint {
  echo -e "\033[1;32m[$(date -Isecond)] ${1}\033[0m"
}

function redprint {
  echo -e "\033[1;31m[$(date -Isecond)] ${1}\033[0m"
}

# Retry container image pull and push
function retry {
  n=0
  until [ "$n" -ge 3 ]; do
    "$@" && break
    n=$((n + 1))
    sleep 10
  done
}

function image_inspect {
  # shellcheck disable=SC2034

  # Test runs on gitlab by default
  CI="${CI:-gitlab}"

  # rhel-bootc and bib for rhel images moved to registry.stage.redhat.io
  if [[ ${TIER1_IMAGE_URL} =~ registry.stage.redhat.io ]]; then
    podman login -u "${REGISTRY_STAGE_USERNAME}" -p "${REGISTRY_STAGE_PASSWORD}" registry.stage.redhat.io
  fi
  if [[ ${TIER1_IMAGE_URL} =~ quay.io/redhat_emp1 ]]; then
    podman login -u "${QUAY_USERNAME}" -p "${QUAY_PASSWORD}" quay.io
  fi
  if [[ "$CI" == "RHTAP" ]]; then
    podman login -u "${KONFLUX_USERNAME}" -p "${KONFLUX_PASSWORD}" "${TIER1_IMAGE_URL%%@*}"
  fi
  # shellcheck disable=SC2034
  # downstream only for bib
  # fedora-bootc image does not have REDHAT_ID, REDHAT_VERSION_ID and CURRENT_COMPOSE_ID Label
  if [[ ${TIER1_IMAGE_URL} =~ fedora-bootc:40 ]]; then
    REDHAT_ID="fedora"
    REDHAT_VERSION_ID="40"
    CURRENT_COMPOSE_ID=""
  elif [[ ${TIER1_IMAGE_URL} =~ fedora-bootc:41 ]]; then
    REDHAT_ID="fedora"
    REDHAT_VERSION_ID="41"
    CURRENT_COMPOSE_ID=""
  elif [[ ${TIER1_IMAGE_URL} =~ fedora-bootc:42 ]]; then
    REDHAT_ID="fedora"
    REDHAT_VERSION_ID="42"
    CURRENT_COMPOSE_ID=""
  elif [[ ${TIER1_IMAGE_URL} =~ fedora-bootc:43 ]]; then
    REDHAT_ID="fedora"
    REDHAT_VERSION_ID="43"
    CURRENT_COMPOSE_ID=""
  elif [[ ${TIER1_IMAGE_URL} =~ fedora-bootc-dev:rawhide ]]; then
    REDHAT_ID="fedora"
    REDHAT_VERSION_ID="43"
  # workaround to get release_id for konflux fedora-bootc images
  elif [[ ${TIER1_IMAGE_URL} =~ fedora-bootc@sha256: ]]; then
    REDHAT_ID="fedora"
    REDHAT_VERSION_ID=$(podman run --rm "${TIER1_IMAGE_URL}" grep -oP '(?<=VERSION_ID=)\d+' /etc/os-release)
    CURRENT_COMPOSE_ID=""
  # konflux for bib image: bootc image builder is hosted on registry.stage.redhat.io for rhel and quay.io for fedora
  elif [[ ${TIER1_IMAGE_URL} =~ bootc-image-builder ]]; then
    VERSION=$(skopeo inspect --retry-times=5 --tls-verify=false docker://"${TIER1_IMAGE_URL}" | jq -r '.Labels.version')
    VENDOR=$(skopeo inspect --retry-times=5 --tls-verify=false docker://"${TIER1_IMAGE_URL}" | jq -r '.Labels.vendor')
    BASE_IMAGE_NAME=$(skopeo inspect --retry-times=5 --tls-verify=false docker://"${TIER1_IMAGE_URL}" | jq -r '.Labels.name')
    if [[ ${VERSION} == *beta ]]; then
      BETA="-beta"
    else
      BETA=""
    fi
    if [[ "$VENDOR" == Red\ Hat* ]]; then
      podman login -u "${REGISTRY_STAGE_USERNAME}" -p "${REGISTRY_STAGE_PASSWORD}" registry.stage.redhat.io
      REDHAT_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://registry.stage.redhat.io/rhel${VERSION%%.*}${BETA}/rhel-bootc:${VERSION%-beta}${BETA}" | jq -r '.Labels."redhat.id"')
      REDHAT_VERSION_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://registry.stage.redhat.io/rhel${VERSION%%.*}${BETA}/rhel-bootc:${VERSION%-beta}${BETA}" | jq -r '.Labels."redhat.version-id"')
      CURRENT_COMPOSE_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://registry.stage.redhat.io/rhel${VERSION%%.*}${BETA}/rhel-bootc:${VERSION%-beta}${BETA}" | jq -r '.Labels."redhat.compose-id"')
    # bib upstream image: quay.io/centos-bootc/bootc-image-builder:latest
    # use centos stream bootc image to test bib quay.io/centos-bootc/centos-bootc:stream9
    else
      REDHAT_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://quay.io/centos-bootc/centos-bootc:stream9" | jq -r '.Labels."redhat.id"')
      REDHAT_VERSION_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://quay.io/centos-bootc/centos-bootc:stream9" | jq -r '.Labels."redhat.version-id"')
      CURRENT_COMPOSE_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://quay.io/centos-bootc/centos-bootc:stream9" | jq -r '.Labels."redhat.compose-id"')
    fi
  # RHEL 10.0 nightly image
  elif [[ ${TIER1_IMAGE_URL} =~ images.paas.redhat.com/bootc/rhel-bootc ]]; then
    REDHAT_ID=rhel
    REDHAT_VERSION_ID=10.0
    CURRENT_COMPOSE_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://${TIER1_IMAGE_URL}" | jq -r '.Labels."redhat.compose-id"')

  # konflux for rhel-bootc image
  # quay.io/redhat_emp1/image-mode-test:rhel-9-5-bootc image
  else
    REDHAT_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://${TIER1_IMAGE_URL}" | jq -r '.Labels."redhat.id"')
    REDHAT_VERSION_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://${TIER1_IMAGE_URL}" | jq -r '.Labels."redhat.version-id"')
    CURRENT_COMPOSE_ID=$(skopeo inspect --retry-times=5 --tls-verify=false "docker://${TIER1_IMAGE_URL}" | jq -r '.Labels."redhat.compose-id"')
  fi

  # shellcheck disable=SC2034
  if [[ -n ${CURRENT_COMPOSE_ID} ]]; then
    if [[ ${CURRENT_COMPOSE_ID} == *-updates-* ]]; then
      BATCH_COMPOSE="updates/"
    else
      BATCH_COMPOSE=""
    fi
  else
    if [[ "${REDHAT_VERSION_ID}" == *-beta ]]; then
      BATCH_COMPOSE=""
      CURRENT_COMPOSE_ID=latest-RHEL-$REDHAT_VERSION_ID
    else
      BATCH_COMPOSE="updates/"
      CURRENT_COMPOSE_ID=latest-RHEL-$REDHAT_VERSION_ID
    fi
  fi
}

function deploy_libvirt_network {
  greenprint "Start firewalld"
  sudo systemctl enable --now firewalld

  greenprint "🚀 Starting libvirt daemon"
  sudo systemctl start libvirtd
  sudo virsh list --all >/dev/null

  # Set a customized dnsmasq configuration for libvirt so we always get the
  # same address on boot up.
  greenprint "💡 Setup libvirt network"
  sudo tee /tmp/integration.xml >/dev/null <<EOF
<network xmlns:dnsmasq='http://libvirt.org/schemas/network/dnsmasq/1.0'>
<name>integration</name>
<uuid>1c8fe98c-b53a-4ca4-bbdb-deb0f26b3579</uuid>
<forward mode='nat'>
    <nat>
    <port start='1024' end='65535'/>
    </nat>
</forward>
<bridge name='integration' zone='trusted' stp='on' delay='0'/>
<mac address='52:54:00:36:46:ef'/>
<ip address='192.168.100.1' netmask='255.255.255.0'>
    <dhcp>
    <range start='192.168.100.2' end='192.168.100.254'/>
    <host mac='34:49:22:B0:83:30' name='vm-1' ip='192.168.100.50'/>
    <host mac='34:49:22:B0:83:31' name='vm-2' ip='192.168.100.51'/>
    <host mac='34:49:22:B0:83:32' name='vm-3' ip='192.168.100.52'/>
    </dhcp>
</ip>
<dnsmasq:options>
    <dnsmasq:option value='dhcp-vendorclass=set:efi-http,HTTPClient:Arch:00016'/>
    <dnsmasq:option value='dhcp-option-force=tag:efi-http,60,HTTPClient'/>
    <dnsmasq:option value='dhcp-boot=tag:efi-http,&quot;http://192.168.100.1/httpboot/EFI/BOOT/BOOTX64.EFI&quot;'/>
</dnsmasq:options>
</network>
EOF
  if ! sudo virsh net-info integration >/dev/null 2>&1; then
    sudo virsh net-define /tmp/integration.xml
  fi
  if [[ $(sudo virsh net-info integration | grep 'Active' | awk '{print $2}') == 'no' ]]; then
    sudo virsh net-start integration
  fi
  sudo rm -f /tmp/integration.xml
}

function expand_rootfs {
  greenprint "Expand rootfs"
  if ! lsblk | grep -q "lvm"; then
    echo "System is not using LVM."
    return
  fi
  root_lv=$(sudo lvdisplay | grep 'LV Path' | grep 'root' | awk '{print $3}' || true)
  if [ -z "$root_lv" ]; then
    echo "Root logical volume not found."
    exit 1
  fi
  echo "Extending root logical volume: $root_lv"
  # Remove swap logical volumes
  swap_lv=$(sudo lvdisplay | grep 'LV Path' | grep 'swap' | awk '{print $3}' || true)
  if [ -n "$swap_lv" ]; then
    sudo swapoff -a
    sudo lvremove -f "$swap_lv"
  fi
  # Remove home logical volumes
  home_lv=$(sudo lvdisplay | grep 'LV Path' | grep 'home' | awk '{print $3}' || true)
  if [ -n "$home_lv" ]; then
    # Extract VG and LV names
    vg_name=$(echo "$home_lv" | awk -F'/' '{print $3}')
    lv_name=$(echo "$home_lv" | awk -F'/' '{print $4}')
    # Escape dashes
    escaped_vg_name=${vg_name//-/--}
    escaped_lv_name=${lv_name//-/--}
    # Construct the device mapper path
    dev_mapper_path="/dev/mapper/${escaped_vg_name}-${escaped_lv_name}"
    mount_point=$(mount | grep "$dev_mapper_path" | awk '{print $3}' || true)
    if [ -n "$mount_point" ]; then
      echo "Unmounting logical volume: $dev_mapper_path from $mount_point"
      sudo umount "$mount_point"
    else
      echo "Logical volume: $dev_mapper_path is not mounted."
    fi
    echo "Removing logical volume: $home_lv"
    sudo lvremove -f "$home_lv"
  fi
  sudo lvextend -l +100%FREE "$root_lv"
  # Get filesystem type
  fs_type=$(df -T | grep '/$' | awk '{print $2}')
  # Resize filesystem based on type
  case $fs_type in
  ext4)
    sudo resize2fs "$root_lv"
    ;;
  xfs)
    sudo xfs_growfs /
    ;;
  *)
    echo "Unsupported filesystem type: $fs_type"
    exit 1
    ;;
  esac
}
