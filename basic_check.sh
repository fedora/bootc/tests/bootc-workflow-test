#!/bin/bash
set -euo pipefail

rpm -q bootc
bootc --version
rpm-ostree --version
podman --version
grep -oP '(?<=VERSION_ID=")[^"]+' /etc/os-release
