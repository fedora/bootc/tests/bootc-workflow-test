#!/usr/bin/env bash

bootcCreateDocument() {
  DATA=$(
    cat <<EOF
    {
      "fields": {
        "prName": { "stringValue": "$PR_NAME" },
        "scenario": { "stringValue": "$SCENARIO" },
        "commit": { "stringValue": "$SHA_URL" },
        "onPush": { "booleanValue": $ONPUSH },
        "optional": { "booleanValue": $OPTIONAL },
        "prLog": { "stringValue": "$PR_LOG" },
        "image": { "stringValue": "$IMAGE" },
        "snapshot": { "stringValue": "$SNAPSHOT_NAME" },
        "created": { "timestampValue": "$PR_START_TIME_STD" },
        "tfArtifacts": { "stringValue": "$TF_ARTIFACTS_URL" },
        "result": { "stringValue": "$OVERALL_RESULT" },
        "queueTime": { "integerValue": ${QUEUE_SECONDS_INT:-0} },
        "rerun": { "booleanValue": ${RERUN:-"false"} }
      }
    }
EOF
  )

  DOC_URL="https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/$APPLICATION"
  PARAM="?documentId=$PR_NAME"
  curl -s \
    -X POST \
    -d "$DATA" \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "${DOC_URL}${PARAM}"
}

patchTFResult() {
  DATA=$(
    cat <<EOF
    {
      "fields": {
        "tfArtifacts": { "stringValue": "$TF_ARTIFACTS_URL" },
        "result": { "stringValue": "$OVERALL_RESULT" },
        "queueTime": { "integerValue": ${QUEUE_SECONDS_INT:-0} },
      }
    }
EOF
  )

  DOC_URL="https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/$APPLICATION/$PR_NAME"
  PARAM="?updateMask.fieldPaths=tfArtifacts&updateMask.fieldPaths=result&updateMask.fieldPaths=queueTime"
  curl -s \
    -X PATCH \
    -d "$DATA" \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "${DOC_URL}${PARAM}"
}

patchReRun() {
  DATA=$(
    cat <<EOF
    {
      "fields": {
        "rerun": { "booleanValue": true },
      }
    }
EOF
  )

  DOC_URL="https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/$APPLICATION/$PR_NAME"
  PARAM="?updateMask.fieldPaths=rerun"
  curl -s \
    -X PATCH \
    -d "$DATA" \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "${DOC_URL}${PARAM}"
}

# shellcheck disable=SC1091
getToken() {
  PROJECT_ID=virt-qe
  DATABASE_ID="(default)"
  if [[ ! -d ~/venv/google-auth-oauthlib ]]; then
    python3 -m venv ~/venv/google-auth-oauthlib
    . "$HOME/venv/google-auth-oauthlib/bin/activate"
    pip install -U google-auth-oauthlib
    deactivate
  fi
  . "$HOME"/venv/google-auth-oauthlib/bin/activate
  curl -sLO https://gitlab.com/fedora/bootc/tests/bootc-workflow-test/-/raw/main/.tekton/scripts/firestore_access_token.py
  if echo "$GCP_SERVICE_ACCOUNT_FILE_B64" | base64 -d >/dev/null 2>&1; then
    echo "$GCP_SERVICE_ACCOUNT_FILE_B64" | base64 -d >./google_auth.json
  else
    echo "$GCP_SERVICE_ACCOUNT_FILE_B64" >./google_auth.json
  fi
  TOKEN=$(python3 ./firestore_access_token.py ./google_auth.json)
  rm -f ./firestore_access_token.py
  rm -f ./google_auth.json
  deactivate
}

get() {
  curl -s \
    -H "authorization: Bearer $TOKEN" \
    "https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/users/"
}

patch() {
  DATA=$(
    cat <<EOF
    {
      "fields": {
        "field1": { "nullValue": null },
        "field2": { "booleanValue": true },
        "field3": { "integerValue": 23 },
        "field4": { "doubleValue": 11.23 },
        "field5": { "timestampValue": "2024-11-19T11:01:23Z" },
        "field6": { "stringValue": "value1" },
        "field7": { "bytesValue": "aGVsbG8K" },
        "field8": { "referenceValue": "projects/$PROJECT_ID/databases/$DATABASE_ID/documents/cities/BJ" },
        "field9": { "arrayValue": {
                       "values": [
                         { "stringValue": "value1" },
                         { "stringValue": "value2" }
                       ]
                  }},
        "field10": { "mapValue": {
                        "fields": {
                          "key1": { "stringValue": "newValue1" },
                          "key2": { "integerValue": 42 },
                          "key3": { "booleanValue": true }
                        }
                   }}
      }
    }
EOF
  )

  DOC_URL="https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/users/temp"
  PARAM="?updateMask.fieldPaths=field1&updateMask.fieldPaths=field2"
  PARAM=""
  curl -s \
    -X PATCH \
    -d "$DATA" \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "${DOC_URL}${PARAM}"
}

structuredQueryByFilter() {
  DATA=$(
    cat <<EOF
    {
      "structuredQuery": {
        "from":[
          {
            "collectionId": "$APPLICATION"
          }
        ],
        "where": {
          $FILTER
        }
      }
    }
EOF
  )

  curl -s \
    -X POST \
    -d "$DATA" \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents:runQuery"
}

structuredQuery() {
  DATA=$(
    cat <<EOF
    {
      "structuredQuery": {
        "from":[
          {
            "collectionId": "$APPLICATION"
          }
        ],
        "where": {
          "compositeFilter": {
            "op": "AND",
            "filters": [
              {
                "fieldFilter": {
                  "field": {
                    "fieldPath": "rerun"
                  },
                  "op": "EQUAL",
                  "value": {
                    "booleanValue": false
                  }
                }
              },
              {
                "fieldFilter": {
                  "field": {
                    "fieldPath": "result"
                  },
                  "op": "EQUAL",
                  "value": {
                    "stringValue": "check-tf-queue-time"
                  }
                }
              }
            ]
          }
        }
      }
    }
EOF
  )

  curl -s \
    -X POST \
    -d "$DATA" \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents:runQuery"
}

createDocument() {
  DOC_URL="https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/$1"
  PARAM="?documentId=$2"
  PARAM=""
  curl -s \
    -X POST \
    -d '{}' \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "${DOC_URL}${PARAM}"
}

listDocuments() {
  DOC_URL="https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents/$APPLICATION"
  PARAM="?mask.fieldPaths=prName"
  curl -s \
    -X GET \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "${DOC_URL}${PARAM}"
}

listCollectionIds() {
  curl -s \
    -X POST \
    -H "authorization: Bearer $TOKEN" \
    -H "Content-Type: application/json" \
    "https://firestore.googleapis.com/v1/projects/$PROJECT_ID/databases/$DATABASE_ID/documents:listCollectionIds"
}

checkCollectionId() {
  listCollectionIds | jq "any(.collectionIds[]; . == \"$1\")"
}

assertCollectionId() {
  if [[ $(checkCollectionId "$1") == "false" ]]; then
    createDocument "$1"
  fi
}

getToken
# structuredQuery
# bootcCreateDocument
