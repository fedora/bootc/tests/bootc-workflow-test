#!/usr/bin/env python3
#
# pip install google-auth-oauthlib
#

import sys
from google.oauth2 import service_account
from google.auth.transport.requests import Request


def main():
    if len(sys.argv) != 2:
        print("Usage: python "+sys.argv[0]+" <filename>")
        sys.exit(1)

    service_account_file = sys.argv[1]

    scopes = [
        "https://www.googleapis.com/auth/datastore",
        "https://www.googleapis.com/auth/cloud-platform"
    ]

    credentials = service_account.Credentials.from_service_account_file(
        service_account_file, scopes=scopes)

    request = Request()
    credentials.refresh(request)
    access_token = credentials.token
    print(access_token)


if __name__ == "__main__":
    main()
