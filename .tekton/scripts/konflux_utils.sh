#!/usr/bin/env bash

PR_START_TIME=$(date -u '+%s')

installAlpinePkgs() {
  sed -i -e 's/v3\.15/v3\.16/g' /etc/apk/repositories
  apk update
  apk add --upgrade apk-tools
  apk upgrade --available
  apk add skopeo jq grep curl libxml2-utils rsync
}

# shellcheck disable=SC2034
getEnvSecrets() {
  TESTING_FARM_API_TOKEN=$(cat /etc/secrets/testing-farm-token)
  export TESTING_FARM_API_TOKEN
  DOWNLOAD_NODE=$(cat /etc/secrets/DOWNLOAD_NODE)
  REGISTRY_STAGE_USERNAME=$(cat /etc/secrets/REGISTRY_STAGE_USERNAME)
  REGISTRY_STAGE_PASSWORD=$(cat /etc/secrets/REGISTRY_STAGE_PASSWORD)
  RHC_AK=$(cat /etc/secrets/RHC_AK)
  RHC_ORGID=$(cat /etc/secrets/RHC_ORGID)
  GCP_SERVICE_ACCOUNT_FILE_B64=$(cat /etc/secrets/GCP_SERVICE_ACCOUNT_FILE_B64)
  SLACK_INCOMING_WEBHOOK=$(cat /etc/secrets/SLACK_INCOMING_WEBHOOK)
  OSCI_METRICS_URL=$(cat /etc/secrets/OSCI_METRICS_URL)
  GITLAB_PAT=$(cat /etc/pac/password)
}

# shellcheck disable=SC2034
getEnv() {
  APPLICATION=$(grep -oP '(?<=application=")[^"]+' </etc/podinfo/labels)
  PR_NAME=$(grep -oP '(?<=pipelineRun=")[^"]+' </etc/podinfo/labels)
  COMPONENT=$(grep -oP '(?<=component=")[^"]+' </etc/podinfo/labels)
  OPTIONAL=$(grep -oP '(?<=optional=")[^"]+' </etc/podinfo/labels)
  SCENARIO=$(grep -oP '(?<=scenario=")[^"]+' </etc/podinfo/labels)
  SNAPSHOT_NAME=$(grep -oP '(?<=snapshot=")[^"]+' </etc/podinfo/labels)
  LOG_URL=$(grep -oP '(?<=log-url=")[^"]+' </etc/podinfo/annotations)
  SHA_TITLE=$(grep -oP '(?<=sha-title=")[^"]+' </etc/podinfo/annotations)
  SHA_URL=$(grep -oP '(?<=sha-url=")[^"]+' </etc/podinfo/annotations)
  P_ID=$(grep -oP '(?<=target-project-id=")[^"]+' </etc/podinfo/annotations)
  PR_START_TIME_STD=$(date -d "@$PR_START_TIME" -u '+%Y-%m-%dT%H:%M:%SZ')
  EVENT_TYPE=$(grep -oP '(?<=on-cel-expression=)(?:"?event[^"]+")\K\w+' </etc/podinfo/annotations)
  ONPUSH="false"
  [[ "$EVENT_TYPE" == "push" ]] && ONPUSH="true"
  GITLAB_URL=$(echo "${SNAPSHOT}" | jq -r ".components[]|select(.name==\"$COMPONENT\")|.source.git.url")
  GITLAB_REVISION=$(echo "${SNAPSHOT}" | jq -r ".components[]|select(.name==\"$COMPONENT\")|.source.git.revision")
  IMAGE=$(echo "${SNAPSHOT}" | jq -r ".components[]|select(.name==\"$COMPONENT\")|.containerImage")
  COUNT=$(echo "${SNAPSHOT}" | jq -r ".components|map(select(.name==\"$COMPONENT\"))|length")
  PR_LOG="${LOG_URL%/*}/${PR_NAME}/logs"
}

# shellcheck disable=SC2034
parseImage() {
  skopeo inspect docker://"$IMAGE" >skopeo_inspect.json
  [[ -z "$(cat skopeo_inspect.json)" ]] && exit 1
  IMAGE_NAME=$(echo "${IMAGE##*/}" | cut -d @ -f 1)
  IMAGE_TAG=$(echo "${IMAGE##*/}" | cut -d : -f 2)
  IFS=: read -r KONFLUX_USERNAME KONFLUX_PASSWORD < <(jq -r ".auths.\"${IMAGE%%@*}\".auth" /root/.docker/config.json | base64 -d)
  if [[ $IMAGE_NAME =~ ^rhel-bootc || $IMAGE_NAME =~ ^centos-bootc || $IMAGE_NAME =~ ^fedora-bootc ]]; then
    REDHAT_ID=$(jq -r '.Labels."redhat.id"' skopeo_inspect.json)
    REDHAT_VERSION_ID=$(jq -r '.Labels."redhat.version-id"' skopeo_inspect.json)
    if [[ "${REDHAT_ID}" == "rhel" ]]; then
      if [[ ${REDHAT_VERSION_ID} == *-beta ]]; then
        COMPOSE_STAGE="-Beta"
      else
        COMPOSE_STAGE=""
      fi
      if [[ ${REDHAT_VERSION_ID%%.*} -gt 9 ]]; then
        COMPOSE=RHEL-${REDHAT_VERSION_ID%-beta}${COMPOSE_STAGE}-Nightly
      else
        COMPOSE=RHEL-${REDHAT_VERSION_ID%-beta}.0${COMPOSE_STAGE}-Nightly
      fi
      DISTRO=rhel-${REDHAT_VERSION_ID}
    elif [[ "${REDHAT_ID}" == "centos" ]]; then
      COMPOSE=CentOS-Stream-${REDHAT_VERSION_ID}
      # FIXME: workaround for "No package epel-release available"
      if [[ "${REDHAT_VERSION_ID}" == "10" ]]; then
        COMPOSE=CentOS-Stream-9
      fi
      DISTRO=centos-${REDHAT_VERSION_ID}
    else
      COMPOSE=Fedora-41
      DISTRO=fedora-41
    fi
    [[ -z ${TMT_TAG} ]] && TMT_TAG=stable
  elif [[ $IMAGE_NAME =~ ^bootc-image-builder ]]; then
    BASE_IMAGE_NAME=$(jq -r '.Labels.name' skopeo_inspect.json)
    BASE_IMAGE_VENDOR=$(jq -r '.Labels.vendor' skopeo_inspect.json)
    BASE_IMAGE_VERSION=$(jq -r '.Labels.version' skopeo_inspect.json)
    if [[ ${BASE_IMAGE_VENDOR} == Red\ Hat* ]]; then
      if [[ ${BASE_IMAGE_VERSION} == *beta ]]; then
        COMPOSE_STAGE="-Beta"
      else
        COMPOSE_STAGE=""
      fi
      if [[ ${BASE_IMAGE_VERSION%%.*} -gt 9 ]]; then
        COMPOSE=RHEL-${BASE_IMAGE_VERSION%-beta}${COMPOSE_STAGE}-Nightly
      else
        COMPOSE=RHEL-${BASE_IMAGE_VERSION%-beta}.0${COMPOSE_STAGE}-Nightly
      fi
      DISTRO=rhel-${BASE_IMAGE_VERSION}
    else
      COMPOSE=Fedora-${BASE_IMAGE_VERSION}
      DISTRO=fedora-${BASE_IMAGE_VERSION}
    fi
    [[ -z ${TMT_TAG} ]] && TMT_TAG=bib
  else
    echo "Error: unknown container image name: $IMAGE_NAME"
    exit 1
  fi
}

# shellcheck disable=SC2034
parseArch() {
  ARCH=x86_64
  PLATFORM='linux/amd64'
  if [[ ${SCENARIO} == *aarch64 ]]; then
    ARCH=aarch64
    PLATFORM='linux/arm64'
  elif [[ ${SCENARIO} == *s390x ]]; then
    ARCH=s390x
    PLATFORM='linux/s390x'
  elif [[ ${SCENARIO} == *ppc64le ]]; then
    ARCH=ppc64le
    PLATFORM='linux/ppc64le'
  fi
}

# shellcheck disable=SC2034
callTF() {
  TEMPDIR=$(mktemp -d -u)
  testing-farm request \
    --plan-filter "tag: ${TMT_TAG}" \
    --environment TEMPDIR="$TEMPDIR" \
    --environment TIER1_IMAGE_URL="${IMAGE}" \
    --environment DOWNLOAD_NODE="${DOWNLOAD_NODE}" \
    --environment CI="RHTAP" \
    --environment ARCH="${ARCH}" \
    --secret REGISTRY_STAGE_USERNAME="${REGISTRY_STAGE_USERNAME}" \
    --secret REGISTRY_STAGE_PASSWORD="${REGISTRY_STAGE_PASSWORD}" \
    --secret KONFLUX_USERNAME="${KONFLUX_USERNAME}" \
    --secret KONFLUX_PASSWORD="${KONFLUX_PASSWORD}" \
    --secret RHC_AK="${RHC_AK}" \
    --secret RHC_ORGID="${RHC_ORGID}" \
    --git-url "${GIT_URL}" \
    --git-ref "${GIT_REF}" \
    --compose "${COMPOSE}" \
    --arch "${ARCH}" \
    --context "arch=${ARCH}" \
    --context "distro=${DISTRO}" \
    --timeout "240" \
    --no-wait | tee tf_stdout.txt

  R_ID=$(grep -oP '(?<=https://api.dev.testing-farm.io/v0.1/requests/)[0-9a-z-]+' tf_stdout.txt)
  TF_ARTIFACTS_URL="https://api.dev.testing-farm.io/v0.1/requests/${R_ID}"
}

# shellcheck disable=SC2034
getMRIID() {
  if [[ ${GITLAB_URL} == https://gitlab.com/* ]]; then
    P_ID=$(curl -s --header "Private-Token: ${GITLAB_PAT}" -X GET "https://gitlab.com/api/v4/projects?membership=1&search=$(basename "${GITLAB_URL}")" | jq -r ".[]|select(.web_url==\"${GITLAB_URL%/}\") | if .forked_from_project!=null then .forked_from_project.id else .id end")
    IID=$(curl -s --header "Private-Token: ${GITLAB_PAT}" -X GET "https://gitlab.com/api/v4/projects/${P_ID}/repository/commits/${GITLAB_REVISION}/merge_requests" | jq -r ".[].iid")
  fi
}

waitForTFResult() {
  PREV_STATE="none"
  STATE="none"
  while true; do
    CUR_TIME=$(date -u '+%s')
    DURATION_MIN=$(((CUR_TIME - PR_START_TIME) / 60))
    if [[ $DURATION_MIN -gt $TIMEOUT ]]; then
      echo "Timeout! Failed to finish within \"${TIMEOUT}mins\"."
      break
    fi
    STATE=$(curl --retry 10 --retry-connrefused --connect-timeout 10 --retry-delay 30 -s "https://api.dev.testing-farm.io/v0.1/requests/$R_ID" | jq -r '.state')
    if [ "$STATE" = "complete" ] || [ "$STATE" = "error" ]; then
      echo "Done! The current state is \"$STATE\"."
      break
    fi
    if [ "$STATE" != "$PREV_STATE" ]; then
      echo "The current state is \"$STATE\"."
      echo "Waiting for Testing Farm..."
    fi
    PREV_STATE="$STATE"
    sleep 90
  done
}

# shellcheck disable=SC2034
getTFQueueTime() {
  QUEUE_SECONDS=0
  INTERVAL=$1
  NOW_SEC=$(date -u "+%s")

  while [[ ${QUEUE_SECONDS%.*} -le 0 ]]; do
    PRE_SEC=$((NOW_SEC - INTERVAL))
    METRICS=$(curl -skL "${OSCI_METRICS_URL}" -H 'content-type: application/json' \
      -d "{\"queries\":[{\"datasource\":{\"type\":\"prometheus\"},\"expr\":\"rate(tf_requests_queued_time_seconds_sum{ranch=\\\"redhat\\\"}[\$__rate_interval]) / rate(tf_requests_queued_time_seconds_count{ranch=\\\"redhat\\\"}[\$__rate_interval])\n\",\"utcOffsetSec\":28800,\"datasourceId\":4,\"intervalMs\":15000,\"maxDataPoints\":1488}],\"from\":\"${PRE_SEC}000\",\"to\":\"${NOW_SEC}000\"}")
    QUEUE_SECONDS=$(echo "${METRICS}" | jq -r '[.results.A.frames[0].data.values[1][] // 0]|add/length*100|round/100')
    QUEUE_SECONDS_INT=${QUEUE_SECONDS%.*}
    INTERVAL=$((INTERVAL * 2))
  done
}

commentMROnFailure() {
  QUEUE_SECONDS=$(curl -sk "https://api.dev.testing-farm.io/v0.1/requests/$R_ID" | jq -r '.queued_time')
  QUEUE_SECONDS_INT=${QUEUE_SECONDS%.*}
  QUEUE_TIME=$(printf '%dh:%dm:%ds\n' $((QUEUE_SECONDS_INT / 3600)) $((QUEUE_SECONDS_INT % 3600 / 60)) $((QUEUE_SECONDS_INT % 60)))
  QUEUE_MSG=""
  if [[ ${QUEUE_SECONDS_INT} -gt 900 ]]; then
    QUEUE_MSG="QUEUED TIME: $QUEUE_TIME, the test failure might because of this long queued time which leads to the timeout.<br />"
  fi
  NOTES=""
  if [[ "$STATE" == "running" ]]; then
    NOTES=$(curl -sk https://artifacts.osci.redhat.com/testing-farm/"$R_ID"/results.xml | xmllint --xpath '//testsuite/@stage' - | cut -d \" -f 2 | sort | uniq -c | xargs echo)
  fi
  PROV_MSG=""
  if [[ ${NOTES} == *created* ]] || [[ ${NOTES} == *guest_provisioning* ]] || [[ ${NOTES} == *guest_setup* ]]; then
    PROV_MSG="Test status: $NOTES, the test failure might because of the long time to reserve a system, if the queued time is not long.<br />"
  fi
  if [[ -n ${IID} ]] && [[ ${OPTIONAL} == "false" ]]; then
    curl -s --header "Private-Token: ${GITLAB_PAT}" -X POST "https://gitlab.com/api/v4/projects/${P_ID}/merge_requests/${IID}/notes" \
      --header "Content-Type: application/json" \
      --data-raw "{ \"body\": \"SCENARIO: ${SCENARIO}<br />PipelineRun: $PR_LOG<br />Result: $OVERALL_RESULT<br />${QUEUE_MSG}${PROV_MSG}Testing farm artifacts: $TF_ARTIFACTS_URL<br />\" }"
  fi
}
