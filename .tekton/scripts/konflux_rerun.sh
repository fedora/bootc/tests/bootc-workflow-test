#!/bin/bash

# shellcheck disable=SC2034
# FIXME:
GCP_SERVICE_ACCOUNT_FILE_B64=$(cat "$HOME"/etc/virt-qe-fd13804cd81a.json)

# FIXME:
# wget -q https://gitlab.com/fedora/bootc/tests/bootc-workflow-test/-/raw/main/.tekton/scripts/firestore_utils.sh
# wget -q https://gitlab.com/fedora/bootc/tests/bootc-workflow-test/-/raw/main/.tekton/scripts/konflux_utils.sh
# . ./firestore_utils.sh
# . ./konflux_utils.sh
# REGEX_PATTERN_FILE="/tmp/regex_pattern.txt"
# wget -q https://gitlab.com/fedora/bootc/tests/bootc-workflow-test/-/raw/main/.tekton/scripts/konflux_utils.sh -o "$REGEX_PATTERN_FILE"

REGEX_PATTERN_FILE="$HOME/src/bootc-workflow-test/.tekton/scripts/regex_pattern.txt"
# shellcheck disable=SC1091
. "$HOME"/src/bootc-workflow-test/.tekton/scripts/firestore_utils.sh
# shellcheck disable=SC1091
. "$HOME"/src/bootc-workflow-test/.tekton/scripts/konflux_utils.sh

trap cleanup 2 3 15

cleanup() {
  # TODO:
  # rm -f "$REGEX_PATTERN_FILE"
  exit
}

reRun() {
  kubectl label snapshot "$SNAPSHOT_NAME" test.appstudio.openshift.io/run="$SCENARIO"
}

reRunSkip() {
  DATA=$(
    cat <<EOF
'[{"op": "replace", "path": "/spec/params", "value":[{"name":"SKIP","value":"$IMAGE"}]}]'
EOF
  )
  eval kubectl patch its "$SCENARIO" --type='json' -p "$DATA"
  reRun
}

checkFinished() {
  FINISHED="false"
  PR_START_TIME=$(date -d "$PR_START_TIME_STD" -u +"%s")
  NOW=$(date -u '+%s')
  if (((NOW - PR_START_TIME) > (6 * 60 * 60))); then
    FINISHED="true"
  else
    FINISHED_STATUS=$(kubectl get pipelinerun "$PR_NAME" -o json 2>/dev/null | jq -r '.status.conditions[].reason')
    if [[ "$FINISHED_STATUS" != "Running" ]]; then
      FINISHED="true"
    fi
  fi
}

queryNewerOnPushPR() {
  FILTER=$(
    cat <<EOF
    "fieldFilter": {
      "field": {"fieldPath": "created"},
      "op": "GREATER_THAN",
      "value": {"timestampValue": "$PR_START_TIME_STD"}
    }
EOF
  )
  RESPONSE=$(structuredQueryByFilter)
  NEWER_PR_COUNT=$(echo "$RESPONSE" | jq -r "[.[] | select(.document.fields.onPush.booleanValue==true and .document.fields.scenario.stringValue==\"$SCENARIO\")]|length")
}

checkReRun() {
  CHECK="false"
  if [[ "$ONPUSH" == "true" ]]; then
    queryNewerOnPushPR
    if [[ $NEWER_PR_COUNT -eq 0 ]]; then
      CHECK="true"
    fi
  else
    [[ -z "$GITLAB_PAT" ]] && GITLAB_PAT=$(kubectl get secrets pipelines-as-code-secret -o json | jq -r '.data.password' | base64 -d)
    if [[ "$SHA_URL" == https://gitlab.com/* ]]; then
      GITLAB_URL=$(echo "$SHA_URL" | grep -oP '.*(?=/-/commit.*)')
      GITLAB_REVISION=${SHA_URL##*/}
      P_ID=$(curl -s --header "Private-Token: ${GITLAB_PAT}" -X GET "https://gitlab.com/api/v4/projects?membership=1&search=${GITLAB_URL##*/}" | jq -r ".[]|select(.web_url==\"${GITLAB_URL%/}\") | if .forked_from_project!=null then .forked_from_project.id else .id end")
      MR_STATE=$(curl -s --header "Private-Token: ${GITLAB_PAT}" -X GET "https://gitlab.com/api/v4/projects/${P_ID}/repository/commits/${GITLAB_REVISION}/merge_requests" | jq -r ".[].state")
      if [[ "$MR_STATE" != "merged" ]]; then
        CHECK="true"
      fi
    fi
  fi
}

checkLatestTFResultRerunIfPass() {
  # Failed before sending TF request, could be snapshot image parsing issues
  if [[ -z "$TF_ARTIFACTS_URL" ]]; then
    checkReRun
    if [[ "$CHECK" == "true" ]]; then
      reRun
    fi
    patchReRun
  fi
  if [[ "$TF_ARTIFACTS_URL" == https://artifacts.* ]]; then
    R_ID=${TF_ARTIFACTS_URL##*/}
    TF_RESPONSE=$(curl -sL https://api.testing-farm.io/v0.1/requests/"$R_ID")
  else
    TF_RESPONSE=$(curl -sL "$TF_ARTIFACTS_URL")
  fi
  OVERALL_RESULT=$(echo "$TF_RESPONSE" | jq -r '.result.overall // ""')
  if [[ -n "$OVERALL_RESULT" ]]; then
    QUEUE_SECONDS=$(echo "$TF_RESPONSE" | jq -r .queued_time)
    QUEUE_SECONDS_INT=${QUEUE_SECONDS%.*}
    patchTFResult
    if [[ "$OVERALL_RESULT" == "passed" ]]; then
      checkReRun
      if [[ "$CHECK" == "true" ]]; then
        reRunSkip
      fi
      patchReRun
    elif [[ "$OVERALL_RESULT" == "skipped" ]]; then
      patchReRun
    fi
  else
    TF_START_TIME_STD=$(echo "$TF_RESPONSE" | jq -r .created)
    TF_START_TIME=$(date -d "$TF_START_TIME_STD" -u +"%s")
    NOW=$(date -u '+%s')
    if (((NOW - TF_START_TIME) > (4 * 60 * 60))); then
      checkReRun
      if [[ "$CHECK" == "true" ]]; then
        reRun
      fi
      patchReRun
    fi
  fi
}

# shellcheck disable=SC2034
rerunTFQueueTaskUnfinished() {
  # FIXME:
  echo "---> rerunTFQueueTaskUnfinished"
  [[ -z "$OSCI_METRICS_URL" ]] && OSCI_METRICS_URL=$(oc get secrets bootc-workflow-test -o yaml | yq .data.OSCI_METRICS_URL | base64 -d)
  getTFQueueTime $((15 * 60))
  if [[ ${QUEUE_SECONDS_INT} -gt $((15 * 60)) ]]; then
    QUEUE_TIME=$(printf '%dh:%dm:%ds\n' $((QUEUE_SECONDS_INT / 3600)) $((QUEUE_SECONDS_INT % 3600 / 60)) $((QUEUE_SECONDS_INT % 60)))
    echo "Queue time: $QUEUE_TIME"
    return
  fi
  FILTER=$(
    cat <<EOF
    "compositeFilter": {
      "op": "AND",
      "filters": [
        {
          "fieldFilter": {
            "field": {"fieldPath": "rerun"},
            "op": "EQUAL",
            "value": {"booleanValue": false}
          }
        },
        {
          "fieldFilter": {
            "field": {"fieldPath": "result"},
            "op": "EQUAL",
            "value": {"stringValue": "check-tf-queue-time"}
          }
        }
      ]
    }
EOF
  )
  RESPONSE=$(structuredQueryByFilter)
  jq -c '.[]|select(has("document"))' < <(echo "$RESPONSE") | while read -r DOCUMENT; do
    read -r PR_NAME SCENARIO IMAGE SNAPSHOT_NAME SHA_URL PR_START_TIME_STD ONPUSH TF_ARTIFACTS_URL <<< \
      "$(jq -r '.document.fields | "\(.prName.stringValue) \(.scenario.stringValue) \(.image.stringValue) \(.snapshot.stringValue) \(.commit.stringValue) \(.created.timestampValue) \(.onPush.booleanValue) \(.tfArtifacts.stringValue)"' < <(echo "$DOCUMENT"))"
    checkFinished
    if [[ "$FINISHED" == "false" ]]; then
      continue
    fi
    echo "$PR_NAME $SNAPSHOT_NAME"
    checkReRun
    if [[ "$CHECK" == "true" ]]; then
      reRun
    fi
    patchReRun
  done
}

# shellcheck disable=SC2034
# testing-farm not finished, update the result if passed, re-run with skip param
# testing-farm not finished, update the result if not passed, wait for updateTFTaskFailures() to handle it
rerunTFTaskUnfinished() {
  # FIXME:
  echo "---> rerunTFTaskUnfinished"
  FILTER=$(
    cat <<EOF
    "compositeFilter": {
      "op": "AND",
      "filters": [
        {
          "fieldFilter": {
            "field": {"fieldPath": "rerun"},
            "op": "EQUAL",
            "value": {"booleanValue": false}
          }
        },
        {
          "fieldFilter": {
            "field": {"fieldPath": "result"},
            "op": "IN",
            "value": {
              "arrayValue": {
                "values": [
                  {"stringValue": "testing-farm"},
                  {"stringValue": "running"},
                ]
              }
            }
          }
        }
      ]
    }
EOF
  )
  RESPONSE=$(structuredQueryByFilter)
  while read -r DOCUMENT; do
    read -r PR_NAME SCENARIO IMAGE SNAPSHOT_NAME SHA_URL PR_START_TIME_STD ONPUSH TF_ARTIFACTS_URL <<< \
      "$(jq -r '.document.fields | "\(.prName.stringValue) \(.scenario.stringValue) \(.image.stringValue) \(.snapshot.stringValue) \(.commit.stringValue) \(.created.timestampValue) \(.onPush.booleanValue) \(.tfArtifacts.stringValue)"' < <(echo "$DOCUMENT"))"
    checkFinished
    if [[ "$FINISHED" == "false" ]]; then
      continue
    fi
    echo "$PR_NAME $SNAPSHOT_NAME"
    checkLatestTFResultRerunIfPass
  done < <(jq -c '.[]|select(has("document"))' < <(echo "$RESPONSE"))
}

checkTFLogRerun() {
  curl -sL "$LOG_FILE" -o /tmp/konflux_tf_output.txt
  if grep -f "$REGEX_PATTERN_FILE" <(cat /tmp/konflux_tf_output.txt) >/dev/null 2>&1; then
    checkReRun
    if [[ "$CHECK" == "true" ]]; then
      reRun
      RERUNED=1
    fi
    patchReRun
  elif ! grep -iE 'error|failed|refused' <(cat /tmp/konflux_tf_output.txt) >/dev/null 2>&1; then
    if [[ "$PR_NAME" == *aarch64* ]]; then
      checkReRun
      if [[ "$CHECK" == "true" ]]; then
        reRun
        RERUNED=1
      fi
      patchReRun
    fi
  else
    echo "Unknown failure: $LOG_FILE"
    [[ -z "$SLACK_INCOMING_WEBHOOK" ]] && SLACK_INCOMING_WEBHOOK=$(kubectl get secrets bootc-workflow-test -o json | jq -r '.data.SLACK_INCOMING_WEBHOOK' | base64 -d)
    curl -s -X POST "${SLACK_INCOMING_WEBHOOK}" \
      --header "Content-Type: application/json" \
      --data-raw "{ \"text\": \"PipelineRun: $PR_NAME\nUnknown failure: $LOG_FILE\" }"
  fi
  rm /tmp/konflux_tf_output.txt
}

# testing-farm is failed/error, re-run it based on error output
# testing-farm is new/queued, do nothing
# testing-farm is passed, update rerun
updateTFTaskFailures() {
  # FIXME:
  echo "---> updateTFTaskFailures"
  echo "-> failed & error"
  FILTER=$(
    cat <<EOF
    "compositeFilter": {
      "op": "AND",
      "filters": [
        {
          "fieldFilter": {
            "field": {"fieldPath": "rerun"},
            "op": "EQUAL",
            "value": {"booleanValue": false}
          }
        },
        {
          "fieldFilter": {
            "field": {"fieldPath": "result"},
            "op": "IN",
            "value": {
              "arrayValue": {
                "values": [
                  {"stringValue": "failed"},
                  {"stringValue": "error"},
                  {"stringValue": "unknown"},
                ]
              }
            }
          }
        }
      ]
    }
EOF
  )
  RESPONSE=$(structuredQueryByFilter)
  while read -r DOCUMENT; do
    read -r PR_NAME SCENARIO IMAGE SNAPSHOT_NAME SHA_URL PR_START_TIME_STD ONPUSH TF_ARTIFACTS_URL <<< \
      "$(jq -r '.document.fields | "\(.prName.stringValue) \(.scenario.stringValue) \(.image.stringValue) \(.snapshot.stringValue) \(.commit.stringValue) \(.created.timestampValue) \(.onPush.booleanValue) \(.tfArtifacts.stringValue)"' < <(echo "$DOCUMENT"))"
    echo "$PR_NAME $SNAPSHOT_NAME"
    if [[ "$TF_ARTIFACTS_URL" == https://artifacts.* ]]; then
      R_ID=${TF_ARTIFACTS_URL##*/}
      TF_RESPONSE=$(curl -sL https://api.testing-farm.io/v0.1/requests/"$R_ID")
    else
      TF_RESPONSE=$(curl -sL "$TF_ARTIFACTS_URL")
    fi
    XUNIT_XML=$(curl -sL "$(echo "$TF_RESPONSE" | jq -r .result.xunit_url)")
    TESTCASE_NOT_EXISTS=$(xmllint --xpath 'not(//testsuite[@result="undefined"]/testcase)' <(echo "$XUNIT_XML"))
    if [[ "$TESTCASE_NOT_EXISTS" == "true" ]]; then
      checkReRun
      if [[ "$CHECK" == "true" ]]; then
        reRun
      fi
      patchReRun
      continue
    fi
    TESTCASE_RESULTS=$(xmllint --xpath '//testsuite[@result="error" or @result="failed"]/testcase/@result' <(echo "$XUNIT_XML") 2>/dev/null | grep -oP '(?<=result=")[^"]*' | uniq)
    if [[ "$TESTCASE_RESULTS" == "passed" ]]; then
      checkReRun
      if [[ "$CHECK" == "true" ]]; then
        reRunSkip
      fi
      patchReRun
      continue
    fi
    LOG_FILES=$(xmllint --xpath '//testsuite[@result="error" or @result="failed"]/testcase//log[@name="testout.log"]/@href' <(echo "$XUNIT_XML") 2>/dev/null | grep -oP '(?<=href=")[^"]*')
    [[ -z "$LOG_FILES" ]] && LOG_FILES=$(xmllint --xpath '//testsuite[@result="error" or @result="failed"]//log[@name="tmt-verbose-log"]/@href' <(echo "$XUNIT_XML") | grep -oP '(?<=href=")[^"]*')
    RERUNED=0
    while read -r LOG_FILE; do
      [[ $RERUNED -eq 0 ]] && checkTFLogRerun
    done < <(echo "$LOG_FILES")
  done < <(jq -c '.[]|select(has("document"))' < <(echo "$RESPONSE"))

  # FIXME:
  echo "-> new & queued"
  FILTER=$(
    cat <<EOF
    "compositeFilter": {
      "op": "AND",
      "filters": [
        {
          "fieldFilter": {
            "field": {"fieldPath": "rerun"},
            "op": "EQUAL",
            "value": {"booleanValue": false}
          }
        },
        {
          "fieldFilter": {
            "field": {"fieldPath": "result"},
            "op": "IN",
            "value": {
              "arrayValue": {
                "values": [
                  {"stringValue": "new"},
                  {"stringValue": "queued"},
                ]
              }
            }
          }
        }
      ]
    }
EOF
  )
  RESPONSE=$(structuredQueryByFilter)
  while read -r DOCUMENT; do
    read -r PR_NAME SCENARIO IMAGE SNAPSHOT_NAME SHA_URL PR_START_TIME_STD ONPUSH TF_ARTIFACTS_URL <<< \
      "$(jq -r '.document.fields | "\(.prName.stringValue) \(.scenario.stringValue) \(.image.stringValue) \(.snapshot.stringValue) \(.commit.stringValue) \(.created.timestampValue) \(.onPush.booleanValue) \(.tfArtifacts.stringValue)"' < <(echo "$DOCUMENT"))"
    checkFinished
    if [[ "$FINISHED" == "false" ]]; then
      continue
    fi
    echo "$PR_NAME $SNAPSHOT_NAME"
    checkLatestTFResultRerunIfPass
  done < <(jq -c '.[]|select(has("document"))' < <(echo "$RESPONSE"))

  # FIXME:
  echo "-> passed"
  FILTER=$(
    cat <<EOF
    "compositeFilter": {
      "op": "AND",
      "filters": [
        {
          "fieldFilter": {
            "field": {"fieldPath": "rerun"},
            "op": "EQUAL",
            "value": {"booleanValue": false}
          }
        },
        {
          "fieldFilter": {
            "field": {"fieldPath": "result"},
            "op": "IN",
            "value": {
              "arrayValue": {
               "values": [
                  {"stringValue": "passed"},
                  {"stringValue": "skipped"},
                ]
              }
            }
          }
        }
      ]
    }
EOF
  )
  RESPONSE=$(structuredQueryByFilter)
  while read -r DOCUMENT; do
    read -r PR_NAME SCENARIO IMAGE SNAPSHOT_NAME SHA_URL PR_START_TIME_STD ONPUSH TF_ARTIFACTS_URL <<< \
      "$(jq -r '.document.fields | "\(.prName.stringValue) \(.scenario.stringValue) \(.image.stringValue) \(.snapshot.stringValue) \(.commit.stringValue) \(.created.timestampValue) \(.onPush.booleanValue) \(.tfArtifacts.stringValue)"' < <(echo "$DOCUMENT"))"
    echo "$PR_NAME $SNAPSHOT_NAME"
    patchReRun
  done < <(jq -c '.[]|select(has("document"))' < <(echo "$RESPONSE"))
}

collectionIds=$(listCollectionIds | jq -r '.collectionIds[]')
echo "$collectionIds" | while read -r APPLICATION; do
  [[ "$APPLICATION" == "bootc-image-builder" ]] && continue
  echo "=== $APPLICATION ==="
  # FIXME:
  if [[ "$APPLICATION" == rhel-bootc-* ]]; then
    export KUBECONFIG=~/etc/kubeconfig_konflux_p02_bootc
  elif [[ "$APPLICATION" == bootc-image-builder-* ]]; then
    export KUBECONFIG=~/etc/kubeconfig_konflux_p02_bootc
  elif [[ "$APPLICATION" == centos-bootc-* ]]; then
    export KUBECONFIG=~/etc/kubeconfig_konflux_rh01
  elif [[ "$APPLICATION" == bootc-image-builder ]]; then
    export KUBECONFIG=~/etc/kubeconfig_konflux_rh01
  else
    echo "Error: Application is unknown!"
  fi

  rerunTFQueueTaskUnfinished
  rerunTFTaskUnfinished
  updateTFTaskFailures
done
