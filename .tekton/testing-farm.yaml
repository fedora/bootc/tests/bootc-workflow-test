apiVersion: tekton.dev/v1
kind: Task
metadata:
  name: testing-farm
spec:
  description: Initiate testing-farm test given a list of container images
  params:
    - name: SNAPSHOT
      description: A list of container images that should undergo testing
    - name: GIT_URL
      description: URL of the GIT repository that contains the tests.
    - name: GIT_REF
      description: Branch of the git repository used containing the tests
    - name: TIMEOUT
      description: Set the timeout for the request in minutes. If the test takes longer than this, it will be terminated.
    - name: TESTING_FARM_API_URL
      description: The testing-farm instance API to use
    - name: SECRET_NAME
    - name: PAC_SECRET_NAME
    - name: DEBUG_OUTPUT
    - name: WAIVE
    - name: SKIP
  results:
    - name: IMAGE_URL
    - name: TESTING_FARM_API_REQUEST_URL
  volumes:
    - name: integration-test-secret
      secret:
        secretName: $(params.SECRET_NAME)
    - name: pipelines-as-code-secret
      secret:
        secretName: $(params.PAC_SECRET_NAME)
    - name: podinfo
      downwardAPI:
        items:
          - path: "labels"
            fieldRef:
              fieldPath: metadata.labels
          - path: "annotations"
            fieldRef:
              fieldPath: metadata.annotations
  steps:
    - name: check-tf-queue-time
      image: quay.io/testing-farm/cli:latest
      volumeMounts:
        - name: integration-test-secret
          mountPath: "/etc/secrets"
          readOnly: true
        - name: pipelines-as-code-secret
          mountPath: "/etc/pac"
          readOnly: true
        - name: podinfo
          mountPath: /etc/podinfo
      env:
        - name: SNAPSHOT
          value: $(params.SNAPSHOT)
        - name: GIT_URL
          value: $(params.GIT_URL)
        - name: GIT_REF
          value: $(params.GIT_REF)
        - name: DEBUG_OUTPUT
          value: $(params.DEBUG_OUTPUT)
        - name: SKIP
          value: $(params.SKIP)
      script: |
        #!/usr/bin/env bash
        [[ "${DEBUG_OUTPUT}" == "1" ]] && set -x

        wget -q "${GIT_URL}/-/raw/${GIT_REF}/.tekton/scripts/konflux_utils.sh"
        wget -q "${GIT_URL}/-/raw/${GIT_REF}/.tekton/scripts/firestore_utils.sh"
        . ./konflux_utils.sh
        installAlpinePkgs
        getEnvSecrets
        . ./firestore_utils.sh
        getEnv
        OVERALL_RESULT="check-tf-queue-time"
        patchTFResult
        if [[ "${SKIP}" == "${IMAGE}" ]]; then
          echo "Skip the tests."
          exit 0
        fi
        OSCI_METRICS_BASE_URL=$(echo ${OSCI_METRICS_URL} | grep -oP 'https?://[^/]+')
        rc=$(curl -sIo /dev/null -w "%{http_code}" ${OSCI_METRICS_BASE_URL})
        if [[ "${rc}" != "200" ]]; then
          echo "Skip check-tf-queue-time due to the connection issue."
          exit 0
        fi

        QUEUE_SECONDS_INT=0
        getTFQueueTime 900
        QUEUE_TIME=$(printf '%dh:%dm:%ds\n' $((QUEUE_SECONDS_INT/3600)) $((QUEUE_SECONDS_INT%3600/60)) $((QUEUE_SECONDS_INT%60)))
        while [[ ${QUEUE_SECONDS_INT} -gt 900 ]]; do
          sleep 60
          getTFQueueTime 900
          QUEUE_TIME=$(printf '%dh:%dm:%ds\n' $((QUEUE_SECONDS_INT/3600)) $((QUEUE_SECONDS_INT%3600/60)) $((QUEUE_SECONDS_INT%60)))
          echo $QUEUE_TIME
        done
        echo "Queue time: $QUEUE_TIME"
        getToken
        patchTFResult
    - name: testing-farm
      image: quay.io/testing-farm/cli:latest
      volumeMounts:
        - name: integration-test-secret
          mountPath: "/etc/secrets"
          readOnly: true
        - name: pipelines-as-code-secret
          mountPath: "/etc/pac"
          readOnly: true
        - name: podinfo
          mountPath: /etc/podinfo
      env:
        - name: SNAPSHOT
          value: $(params.SNAPSHOT)
        - name: GIT_URL
          value: $(params.GIT_URL)
        - name: GIT_REF
          value: $(params.GIT_REF)
        - name: TIMEOUT
          value: $(params.TIMEOUT)
        - name: TESTING_FARM_API_URL
          value: $(params.TESTING_FARM_API_URL)
        - name: DEBUG_OUTPUT
          value: $(params.DEBUG_OUTPUT)
        - name: WAIVE
          value: $(params.WAIVE)
        - name: SKIP
          value: $(params.SKIP)
      script: |
        #!/usr/bin/env bash
        [[ "${DEBUG_OUTPUT}" == "1" ]] && set -x

        if [[ "${SKIP}" == "1" ]]; then
          echo "Skip the tests."
          printf "%s" "Test was skipped" >"$(results.IMAGE_URL.path)"
          printf "%s" "Test was skipped" >"$(results.TESTING_FARM_API_REQUEST_URL.path)"
          exit 0
        fi
        wget -q "${GIT_URL}/-/raw/${GIT_REF}/.tekton/scripts/konflux_utils.sh"
        wget -q "${GIT_URL}/-/raw/${GIT_REF}/.tekton/scripts/firestore_utils.sh"
        . ./konflux_utils.sh
        installAlpinePkgs
        getEnvSecrets
        . ./firestore_utils.sh
        getEnv
        if [[ ${COUNT} -ne 1 ]]; then
          echo "Error: multiple images: ${IMAGES} in this application with component name: ${COMPONENT}"
          exit 1
        fi
        echo "$IMAGE"
        printf "%s" "${IMAGE}" >"$(results.IMAGE_URL.path)"
        if [[ "${SKIP}" == "${IMAGE}" ]]; then
          echo "Skipped the tests."
          printf "%s" "Test was skipped" >"$(results.TESTING_FARM_API_REQUEST_URL.path)"
          OVERALL_RESULT="passed"
          patchTFResult
          patchReRun
          exit 0
        fi
        OVERALL_RESULT="testing-farm"
        patchTFResult
        parseImage
        parseArch
        callTF
        printf "%s" "https://api.dev.testing-farm.io/v0.1/requests/${R_ID}" >"$(results.TESTING_FARM_API_REQUEST_URL.path)"
        patchTFResult
        getMRIID
        waitForTFResult
        if [[ "$STATE" != "new" ]] && [[ "$STATE" != "queued" ]]; then
          TF_ARTIFACTS_URL=$(curl -sk "https://api.dev.testing-farm.io/v0.1/requests/$R_ID" | jq -r '.run.artifacts')
          echo "Artifacts URL is \"$TF_ARTIFACTS_URL\""
        fi
        OVERALL_RESULT="null"
        if [[ "$STATE" == "complete" ]] || [[ "$STATE" == "error" ]]; then
          OVERALL_RESULT=$(curl -sk "https://api.dev.testing-farm.io/v0.1/requests/$R_ID" | jq -r '.result.overall')
          echo "Test result is \"$OVERALL_RESULT\""
        else
          OVERALL_RESULT=$STATE
        fi
        [[ "$OVERALL_RESULT" == "passed" ]] && RERUN="true" && patchReRun
        getToken
        patchTFResult
        if [[ "$OVERALL_RESULT" == "passed" ]] || [[ "$OVERALL_RESULT" == "skipped" ]]; then
          exit 0
        else
          commentMROnFailure
          if [[ "${WAIVE}" == "1" ]]; then
            echo "Waive the failed tests."
            exit 0
          else
            exit 1
          fi
        fi
