#!/bin/bash
set -exuo pipefail

source tools/shared_lib.sh
dump_runner
image_inspect
deploy_libvirt_network

ARCH=$(uname -m)

TEMPDIR=$(mktemp -d)
trap 'rm -rf -- "$TEMPDIR"' EXIT

# SSH configurations
SSH_USER="admin"
SSH_KEY=${TEMPDIR}/id_rsa
ssh-keygen -f "${SSH_KEY}" -N "" -q -t rsa-sha2-256 -b 2048
SSH_KEY_PUB="${SSH_KEY}.pub"
SSH_KEY_PUB_CONTENT="$(cat "$SSH_KEY_PUB")"

INSTALL_CONTAINERFILE=${TEMPDIR}/Containerfile.install
UPGRADE_CONTAINERFILE=${TEMPDIR}/Containerfile.upgrade
QUAY_REPO_TAG="${QUAY_REPO_TAG:-$(
  tr -dc a-z0-9 </dev/urandom | head -c 4
  echo ''
)}"
INVENTORY_FILE="${TEMPDIR}/inventory"
KS_FILE=${TEMPDIR}/ks.cfg
GUEST_IP="192.168.100.50"
FIRMWARE=${FIRMWARE:-"bios"}
PARTITION=${PARTITION:-"standard"}
REGISTRY_IP="192.168.100.1"
REGISTRY_PORT=5000
HTTPD_PATH="/var/www/html"

# Do not enable FIPS except FIPS=enabled
FIPS="${FIPS:-disabled}"

# Only need quay.io login in rhel-9.5-dev image
if [[ "$TIER1_IMAGE_URL" =~ rhel-9.5-dev ]]; then
  greenprint "Login quay.io"
  podman login -u "${QUAY_USERNAME}" -p "${QUAY_PASSWORD}" quay.io
fi

case "$REDHAT_ID" in
"rhel")
  # rhel-bootc images are saved in registry.stage.redhat.io
  greenprint "Login registry.stage.redhat.io"
  podman login -u "${REGISTRY_STAGE_USERNAME}" -p "${REGISTRY_STAGE_PASSWORD}" registry.stage.redhat.io
  # work with old TEST_OS variable value rhel-9-x
  TEST_OS=$(echo "${REDHAT_ID}-${REDHAT_VERSION_ID}" | sed 's/\./-/')
  if [[ "$REDHAT_VERSION_ID" == *-beta ]]; then
    STAGE_POSTFIX="-Public-Beta"
  else
    STAGE_POSTFIX=""
  fi
  # use latest compose if specific compose is not accessible
  RC=$(curl -skIw '%{http_code}' -o /dev/null "http://${DOWNLOAD_NODE}/rhel-${REDHAT_VERSION_ID%%.*}/nightly/${BATCH_COMPOSE}RHEL-${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/${CURRENT_COMPOSE_ID}/STATUS")
  if [[ $RC != "200" ]]; then
    CURRENT_COMPOSE_ID=latest-RHEL-${REDHAT_VERSION_ID%%-beta}
  fi
  sed "s/REPLACE_ME/${DOWNLOAD_NODE}/; s/REPLACE_X/${REDHAT_VERSION_ID%%.*}/; s|REPLACE_BATCH_COMPOSE|${BATCH_COMPOSE}|; s/REPLACE_Y/${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/; s/REPLACE_COMPOSE_ID/${CURRENT_COMPOSE_ID}/" files/rhel.template | tee "${TEMPDIR}/rhel.repo" >/dev/null
  ADD_REPO="COPY rhel.repo /etc/yum.repos.d/rhel.repo"
  ADD_RHC="RUN dnf install -y rhc"
  BOOT_LOCATION="http://${DOWNLOAD_NODE}/rhel-${REDHAT_VERSION_ID%%.*}/nightly/${BATCH_COMPOSE}RHEL-${REDHAT_VERSION_ID%%.*}${STAGE_POSTFIX}/${CURRENT_COMPOSE_ID}/compose/BaseOS/${ARCH}/os/"
  OS_VARIANT="rhel9-unknown"
  BOOT_ARGS="uefi"
  FROM_BOOT_ISO="true"
  # BATCH_COMPOSE will be "" or "updates/"
  if [[ "$BATCH_COMPOSE" == "" ]]; then
    CUT_DIRS=8
  else
    CUT_DIRS=9
  fi
  if [[ "$REDHAT_VERSION_ID" =~ 10.0 ]]; then
    FROM_BOOT_ISO="false"
    # workaround rhel 10 Secure Boot failure
    # e.g. https://access.redhat.com/solutions/5395851
    BOOT_ARGS="uefi,firmware.feature0.name=secure-boot,firmware.feature0.enabled=no"
  fi
  ;;
"centos")
  # work with old TEST_OS variable value centos-stream-9
  TEST_OS=$(echo "${REDHAT_ID}-${REDHAT_VERSION_ID}" | sed 's/-/-stream-/')
  ADD_REPO=""
  ADD_RHC=""
  if [[ "$REDHAT_VERSION_ID" == "9" ]]; then
    # use latest compose if specific compose is not accessible
    RC=$(curl -skIw '%{http_code}' -o /dev/null "https://composes.stream.centos.org/production/${CURRENT_COMPOSE_ID}/STATUS")
    if [[ $RC != "200" ]]; then
      CURRENT_COMPOSE_ID=latest-CentOS-Stream
    fi
    BOOT_LOCATION="https://composes.stream.centos.org/production/${CURRENT_COMPOSE_ID}/compose/BaseOS/${ARCH}/os/"
    OS_VARIANT="centos-stream9"
    FROM_BOOT_ISO="true"
    CUT_DIRS=6
  else
    # use latest compose if specific compose is not accessible
    RC=$(curl -skIw '%{http_code}' -o /dev/null "https://composes.stream.centos.org/stream-10/production/${CURRENT_COMPOSE_ID}/STATUS")
    if [[ $RC != "200" ]]; then
      CURRENT_COMPOSE_ID=latest-CentOS-Stream
    fi
    BOOT_LOCATION="https://composes.stream.centos.org/stream-10/production/${CURRENT_COMPOSE_ID}/compose/BaseOS/${ARCH}/os/"
    OS_VARIANT="centos-stream9"
    FROM_BOOT_ISO="false"
    CUT_DIRS=7
  fi
  BOOT_ARGS="uefi,firmware.feature0.name=secure-boot,firmware.feature0.enabled=no"
  ;;
"fedora")
  TEST_OS="${REDHAT_ID}-${REDHAT_VERSION_ID}"
  ADD_REPO=""
  ADD_RHC=""
  if [[ "$REDHAT_VERSION_ID" == "40" ]]; then
    BOOT_LOCATION="https://dl.fedoraproject.org/pub/fedora/linux/releases/40/Everything/${ARCH}/os/"
    OS_VARIANT="fedora-unknown"
  elif [[ "$REDHAT_VERSION_ID" == "41" ]]; then
    BOOT_LOCATION="https://dl.fedoraproject.org/pub/fedora/linux/releases/41/Everything/${ARCH}/os/"
    OS_VARIANT="fedora-unknown"
  elif [[ "$REDHAT_VERSION_ID" == "42" ]]; then
    BOOT_LOCATION="https://dl.fedoraproject.org/pub/fedora/linux/development/42/Everything/${ARCH}/os/"
    OS_VARIANT="fedora-unknown"
  else
    BOOT_LOCATION="https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Everything/${ARCH}/os/"
    OS_VARIANT="fedora-rawhide"
  fi
  if [[ "$ARCH" == "aarch64" ]]; then
    FROM_BOOT_ISO="false"
  else
    FROM_BOOT_ISO="true"
  fi
  BOOT_ARGS="uefi"
  CUT_DIRS=8
  ;;
*)
  redprint "Variable TIER1_IMAGE_URL is not supported"
  exit 1
  ;;
esac

# FIXME: https://github.com/containers/podman/issues/22813
if [[ "${REDHAT_VERSION_ID%%.*}" == "10" ]]; then
  sed -i 's/^compression_format = .*/compression_format = "gzip"/' /usr/share/containers/containers.conf
fi

greenprint "Generate certificate"
openssl req \
  -newkey rsa:4096 \
  -nodes \
  -sha256 \
  -keyout "${TEMPDIR}/domain.key" \
  -addext "subjectAltName = IP:${REGISTRY_IP}" \
  -x509 \
  -days 365 \
  -out "${TEMPDIR}/domain.crt" \
  -subj "/C=US/ST=Denial/L=Stockholm/O=bootc/OU=bootc-test/CN=bootc-test/emailAddress=bootc-test@bootc-test.org"

greenprint "Update CA Trust"
sudo cp "${TEMPDIR}/domain.crt" "/etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt"
sudo update-ca-trust

greenprint "Deploy registry"
podman run \
  -d \
  --name registry \
  --replace \
  --network host \
  -v "${TEMPDIR}":/certs:z \
  -e REGISTRY_HTTP_ADDR="${REGISTRY_IP}:${REGISTRY_PORT}" \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  quay.io/bootc-test/registry:2.8.3
podman ps -a

# need httpd to host domain.crt used by kickstart
sudo cp "${TEMPDIR}/domain.crt" "$HTTPD_PATH"
sudo systemctl start httpd

TEST_IMAGE_NAME="bootc-workflow-test"
TEST_IMAGE_URL="${REGISTRY_IP}:${REGISTRY_PORT}/${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}"

greenprint "Create $TEST_OS installation Containerfile"
tee "$INSTALL_CONTAINERFILE" >/dev/null <<EOF
FROM "$TIER1_IMAGE_URL"
$ADD_REPO
$ADD_RHC
COPY domain.crt /etc/pki/ca-trust/source/anchors/${REGISTRY_IP}.crt
RUN update-ca-trust && \
    dnf -y install python3 && \
    dnf -y clean all
EOF

greenprint "Check $TEST_OS installation Containerfile"
cat "$INSTALL_CONTAINERFILE"

greenprint "Build $TEST_OS installation container image"
podman build --tls-verify=false --retry=5 --retry-delay=10s -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" -f "$INSTALL_CONTAINERFILE" "$TEMPDIR"

greenprint "Push $TEST_OS installation container image"
retry podman push --tls-verify=false --quiet "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$TEST_IMAGE_URL"

greenprint "💾 Create vm qcow2 files for virt install"
LIBVIRT_UEFI_IMAGE_PATH="/var/lib/libvirt/images/bootc-${TEST_OS}-${FIRMWARE}.qcow2"
sudo qemu-img create -f qcow2 "$LIBVIRT_UEFI_IMAGE_PATH" 20G

greenprint "📑 Generate kickstart file"
tee "$KS_FILE" >/dev/null <<STOPHERE
text
network --bootproto=dhcp --device=link --activate --onboot=on

rootpw --lock --iscrypted locked
user --name=${SSH_USER} --groups=wheel --iscrypted
sshkey --username=${SSH_USER} "$SSH_KEY_PUB_CONTENT"

bootloader --append="console=ttyS0,115200n8"

ostreecontainer --url $TEST_IMAGE_URL

poweroff

%pre
#!/bin/sh
curl -kLO http://192.168.100.1/domain.crt --output-dir /etc/pki/ca-trust/source/anchors
update-ca-trust
%end

%post --log=/var/log/anaconda/post-install.log --erroronfail
# no sudo password for user admin
echo -e 'admin\tALL=(ALL)\tNOPASSWD: ALL' >> /etc/sudoers
%end

zerombr
clearpart --all --initlabel --disklabel=gpt
STOPHERE

case "$PARTITION" in
"standard")
  echo "autopart --nohome --type=plain --fstype=xfs" >>"$KS_FILE"
  ;;
"lvm")
  echo "autopart --nohome --type=lvm --fstype=xfs" >>"$KS_FILE"
  ;;
"btrfs")
  echo "autopart --nohome --type=btrfs" >>"$KS_FILE"
  ;;
*)
  redprint "Variable PARTITION has to be defined"
  exit 1
  ;;
esac

greenprint "Configure console log file"
VIRT_LOG="/tmp/${TEST_OS}-${FIRMWARE}-${PARTITION}-console.log"
[[ -z ${TESTING_FARM_REQUEST_ID+x} ]] || VIRT_LOG="${TMT_TEST_DATA}/../${TEST_OS}-${FIRMWARE}-${PARTITION}-console.log" && sudo chcon -t virt_log_t "${TMT_TEST_DATA}"/../
sudo rm -f "$VIRT_LOG"

# HTTP Boot only runs on RHEL&CentOS with x86_64 + LVM
if [[ "$ARCH" == "x86_64" ]] && [[ "$FIRMWARE" == "uefi" ]] && [[ "$PARTITION" == "lvm" ]] && [[ "$REDHAT_ID" != "fedora" ]]; then
  GRUB_CFG="${HTTPD_PATH}/httpboot/EFI/BOOT/grub.cfg"

  sudo rm -rf "${HTTPD_PATH}/httpboot"
  sudo mkdir -p "${HTTPD_PATH}/httpboot"

  greenprint "📥 Download HTTP boot required files"
  REQUIRED_FOLDERS=("EFI" "images")
  for i in "${REQUIRED_FOLDERS[@]}"; do
    sudo wget -q --inet4-only -r --no-parent -e robots=off -nH --cut-dirs="$CUT_DIRS" --reject "index.html*" --reject "boot.iso" "${BOOT_LOCATION}${i}/" -P "${HTTPD_PATH}/httpboot/"
  done

  greenprint "📝 Update grub.cfg to work with HTTP boot"
  sudo tee -a "${GRUB_CFG}" >/dev/null <<EOF
menuentry 'Install Red Hat Enterprise Linux for Bootc' --class fedora --class gnu-linux --class gnu --class os {
    linuxefi /httpboot/images/pxeboot/vmlinuz inst.stage2=http://192.168.100.1/httpboot inst.ks=http://192.168.100.1/ks.cfg inst.text console=ttyS0,115200
    initrdefi /httpboot/images/pxeboot/initrd.img
}
EOF
  sudo sed -i 's/default="1"/default="3"/' "${GRUB_CFG}"
  sudo sed -i 's/timeout=60/timeout=10/' "${GRUB_CFG}"

  sudo mv "$KS_FILE" "$HTTPD_PATH"

  greenprint "👿 Running restorecon on /var/www/html"
  sudo restorecon -RvF /var/www/html/

  greenprint "Install $TEST_OS via HTTP Boot on UEFI VM"
  sudo virt-install --name="bootc-${TEST_OS}-${FIRMWARE}" \
    --disk path="$LIBVIRT_UEFI_IMAGE_PATH",format=qcow2 \
    --ram 4096 \
    --vcpus 2 \
    --network network=integration,mac=34:49:22:B0:83:30 \
    --boot "$BOOT_ARGS" \
    --pxe \
    --os-variant "$OS_VARIANT" \
    --console file,source.path="$VIRT_LOG" \
    --nographics \
    --noautoconsole \
    --wait \
    --noreboot
else
  if [[ "$FROM_BOOT_ISO" == "true" ]]; then
    greenprint "Download boot.iso"
    curl -O "${BOOT_LOCATION}images/boot.iso"
    sudo mv boot.iso /var/lib/libvirt/images
    LOCAL_BOOT_LOCATION="/var/lib/libvirt/images/boot.iso"
  else
    greenprint "Let's use URL as bootlocation"
    LOCAL_BOOT_LOCATION="$BOOT_LOCATION"
  fi

  if [[ "$FIRMWARE" == "bios" ]]; then
    # RHEL 9.4 and Fedora 40 does not support FIPS
    if [[ "$REDHAT_VERSION_ID" != "9.4" ]] && [[ "$REDHAT_VERSION_ID" != "40" ]]; then
      FIPS="enabled"
    fi
    if [[ "$FIPS" == "enabled" ]]; then
      sed -i 's/115200n8/115200n8 fips=1/' "$KS_FILE"
      EXTRA_ARGS="inst.ks=file:/ks.cfg console=ttyS0,115200 fips=1"
    else
      EXTRA_ARGS="inst.ks=file:/ks.cfg console=ttyS0,115200"
    fi
    greenprint "Install $TEST_OS via anaconda on $FIRMWARE VM"
    sudo virt-install --initrd-inject="$KS_FILE" \
      --extra-args="$EXTRA_ARGS" \
      --name="bootc-${TEST_OS}-${FIRMWARE}" \
      --disk path="$LIBVIRT_UEFI_IMAGE_PATH",format=qcow2 \
      --ram 4096 \
      --vcpus 2 \
      --network network=integration,mac=34:49:22:B0:83:30 \
      --os-variant "$OS_VARIANT" \
      --location "$LOCAL_BOOT_LOCATION" \
      --console file,source.path="$VIRT_LOG" \
      --nographics \
      --noautoconsole \
      --wait \
      --noreboot
  else
    greenprint "Install $TEST_OS via anaconda on $FIRMWARE VM"
    sudo virt-install --initrd-inject="$KS_FILE" \
      --extra-args="inst.ks=file:/ks.cfg console=ttyS0,115200" \
      --name="bootc-${TEST_OS}-${FIRMWARE}" \
      --disk path="$LIBVIRT_UEFI_IMAGE_PATH",format=qcow2 \
      --ram 4096 \
      --vcpus 2 \
      --network network=integration,mac=34:49:22:B0:83:30 \
      --boot "$BOOT_ARGS" \
      --os-variant "$OS_VARIANT" \
      --location "$LOCAL_BOOT_LOCATION" \
      --console file,source.path="$VIRT_LOG" \
      --nographics \
      --noautoconsole \
      --wait \
      --noreboot
  fi
fi

sudo virsh dumpxml "bootc-${TEST_OS}-${FIRMWARE}"

# Start VM.
greenprint "Start VM"
sudo virsh start "bootc-${TEST_OS}-${FIRMWARE}"

greenprint "Wait until VM's IP"
while ! sudo virsh domifaddr "bootc-${TEST_OS}-${FIRMWARE}" | grep ipv4 >/dev/null; do
  sleep 5
  echo "Booting..."
done

wait_for_ssh_up() {
  SSH_OPTIONS=(-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=5)
  SSH_STATUS=$(sudo ssh "${SSH_OPTIONS[@]}" -i "${SSH_KEY}" ${SSH_USER}@"${1}" '/bin/bash -c "echo -n READY"')
  if [[ $SSH_STATUS == READY ]]; then
    echo 1
  else
    echo 0
  fi
}

greenprint "🛃 Checking for SSH is ready to go"
for _ in $(seq 0 30); do
  RESULT=$(wait_for_ssh_up "$GUEST_IP")
  if [[ $RESULT == 1 ]]; then
    echo "SSH is ready now! 🥳"
    break
  fi
  sleep 10
done

greenprint "Prepare inventory file"
tee -a "$INVENTORY_FILE" >/dev/null <<EOF
[guest]
$GUEST_IP

[guest:vars]
ansible_user="$SSH_USER"
ansible_private_key_file="$SSH_KEY"
ansible_ssh_common_args="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

[all:vars]
ansible_python_interpreter=/usr/bin/python3
EOF

greenprint "Prepare ansible.cfg"
export ANSIBLE_CONFIG="${PWD}/playbooks/ansible.cfg"

greenprint "Run ostree checking test"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e bootc_image="$TEST_IMAGE_URL" \
  -e image_label_version_id="${REDHAT_VERSION_ID%-beta}" \
  -e fips="$FIPS" \
  playbooks/check-system.yaml

greenprint "Create upgrade Containerfile"
tee "$UPGRADE_CONTAINERFILE" >/dev/null <<EOF
FROM "$TEST_IMAGE_URL"
RUN dnf -y install wget && \
    dnf -y clean all
EOF

greenprint "Build $TEST_OS upgrade container image"
podman build --tls-verify=false --retry=5 --retry-delay=10s -t "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" -f "$UPGRADE_CONTAINERFILE" "$TEMPDIR"
greenprint "Push $TEST_OS upgrade container image"
retry podman push --tls-verify=false --quiet "${TEST_IMAGE_NAME}:${QUAY_REPO_TAG}" "$TEST_IMAGE_URL"

greenprint "Upgrade $TEST_OS system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  playbooks/upgrade.yaml

greenprint "Run ostree checking test after upgrade"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  -e test_os="$TEST_OS" \
  -e bootc_image="$TEST_IMAGE_URL" \
  -e image_label_version_id="${REDHAT_VERSION_ID%-beta}" \
  -e upgrade="true" \
  -e fips="$FIPS" \
  playbooks/check-system.yaml

greenprint "Rollback $TEST_OS system"
ansible-playbook -v \
  -i "$INVENTORY_FILE" \
  playbooks/rollback.yaml

greenprint "Clean up"
unset ANSIBLE_CONFIG
podman rm -af
sudo virsh destroy "bootc-${TEST_OS}-${FIRMWARE}"
if [[ "$FIRMWARE" == bios ]]; then
  sudo virsh undefine "bootc-${TEST_OS}-${FIRMWARE}"
else
  sudo virsh undefine "bootc-${TEST_OS}-${FIRMWARE}" --nvram
fi
sudo virsh vol-delete --pool images "bootc-${TEST_OS}-${FIRMWARE}.qcow2"

if [[ "$ARCH" == "x86_64" ]] && [[ "$FIRMWARE" == "uefi" ]] && [[ "$PARTITION" == "lvm" ]]; then
  sudo rm -rf "${HTTPD_PATH}/httpboot"
  sudo rm -f "${HTTPD_PATH}/ks.cfg"
else
  sudo rm -f "/var/lib/libvirt/images/boot.iso"
fi

greenprint "🎉 All tests passed."
exit 0
